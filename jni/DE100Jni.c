/*
 * DE100Jni.c
 *
 *  Created on: May 23, 2016
 *      Author: josesuero
 */

#include <jni.h>
#include <stdio.h>
#include <string.h>
#include <dos.h>
#include "com_mstn_DE_api_CapturaEventos.h"
#include "GloryCoLx2010.h"
#include "DE.h"
#include <stdbool.h>
#include <windows.h>
#include "time.h"
#include "assert.h"
#include "cJSON.h"

char* getString(GLYHANDLE handle, int id) {
	char *string;
	if (id != 0)
		GLY_GetStringFromID(handle, id, &string);
	return string;
}

char* intToString(int num) {
	char* buf;
	itoa(num, buf, 10);
	return buf;
}

static JavaVM* g_vm = NULL;
jobject g_obj;
jmethodID g_mid;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved);
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {
	g_vm = vm;
	return JNI_VERSION_1_8;
}

void addlog(char* mess) {
	printf("%s \n", mess);
	fflush(stdout);

	FILE *f = fopen("Z:\\mstn\\DE-100\\DE100Jni\\de100.log", "a+");
	if (f == NULL) {
		printf("Error opening file!\n");
		fflush(stdout);
		return;
	}

	//const char *text = "Write this to the file";
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	fprintf(f, "%d-%d-%d %d:%d:%d\t", tm.tm_year + 1900, tm.tm_mon + 1,
			tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	fprintf(f, "%s \n", mess);

	/*
	 // print integers and floats
	 int i = 1;
	 float py = 3.1415927;
	 fprintf(f, "Integer: %d, float: %f\n", i, py);

	 /// printing s ingle chatacters
	 char c = 'A';
	 fprintf(f, "A character: %c\n", c);
	 */
	//fclose(f);
}

JNIEnv* JNIEnvStart() {
	JNIEnv * g_env;
	// double check it's all ok

	int getEnvStat = (*g_vm)->GetEnv(g_vm, (void **) &g_env, JNI_VERSION_1_8);
	if (getEnvStat == JNI_EDETACHED) {
		//printf("GetEnv: not attached\n");
		if ((*g_vm)->AttachCurrentThread(g_vm, (void **) &g_env, NULL) != 0) {
			printf("Failed to attach\n");
		}
	} else if (getEnvStat == JNI_OK) {
		//
	} else if (getEnvStat == JNI_EVERSION) {
		printf("GetEnv: version not supported\n");
	}

	return g_env;
}

void JNIEnvEnd(JNIEnv* g_env) {
	if ((*g_env)->ExceptionCheck(g_env)) {
		(*g_env)->ExceptionDescribe(g_env);
	}
	(*g_vm)->DetachCurrentThread(g_vm);

}

jobject createDataObj(JNIEnv* env, jvalue args[]) {

	jmethodID cnstrctr;
	jclass c = (*env)->FindClass(env, "com/mstn/DE/data/DEDataObject");
	if (c == 0) {
		printf("Find Class Failed.\n");
	}

	cnstrctr = (*env)->GetMethodID(env, c, "<init>", "(I)V");
	if (cnstrctr == 0) {
		printf("Find method Failed.\n");
	}

	jobject obj = (*env)->NewObjectA(env, c, cnstrctr, args);
	return obj;
}

static cJSON* createDenominationArray(GLYHANDLE h, LPGLYCURRENCY currencies,
		int size) {

	cJSON *denomination = cJSON_CreateArray();
	int i;
	for (i = 0; i < size; i++) {
		cJSON *item;
		item = cJSON_CreateObject();
		//printf("Value: %d\n",currencies[i].ulValue);
		cJSON_AddNumberToObject(item, "Value", currencies[i].ulValue);
		cJSON_AddStringToObject(item, "Category",
				getString(h, currencies[i].Category));
		cJSON_AddNumberToObject(item, "Revision", currencies[i].Rev);
		cJSON_AddStringToObject(item, "CurrencyID", currencies[i].cCurrencyID);
		cJSON_AddNumberToObject(item, "Counts", currencies[i].ulCounts);

		cJSON_AddItemToArray(denomination, item);
	}
	return denomination;
}

static void callbackfn(GLYPARAM* p) {
	cJSON *jsonObj;
	jsonObj = cJSON_CreateObject();

	char* string;

	printf("%d \n", p->ApiID);
	fflush(stdout);

	cJSON_AddNumberToObject(jsonObj, "ApiID", p->ApiID);
	cJSON_AddStringToObject(jsonObj, "Event", getString(p->Handle, p->ApiID));
	cJSON_AddNumberToObject(jsonObj, "Handle", p->Handle);
	cJSON_AddNumberToObject(jsonObj, "ErrorCode", p->dwErrorCode);
	if (p->dwErrorCode != 0) {
		cJSON_AddStringToObject(jsonObj, "Error",
				getString(p->Handle, p->dwErrorCode));
	}
	cJSON_AddNumberToObject(jsonObj, "MessageID", p->dwMessageID);
	cJSON_AddNumberToObject(jsonObj, "RequestID", p->dwRequestID);
	if (p->ApiID != 0) {
		cJSON_AddStringToObject(jsonObj, "Message",
				getString(p->Handle, p->dwMessageID));
	}
	//
	if (p->szParamName) {
		cJSON_AddStringToObject(jsonObj, "ParamName", strdup(p->szParamName));
	}
	cJSON_AddNumberToObject(jsonObj, "Size", p->ulSize);

	int i;

	if (p->ApiID == GLY_DE_DEVICESETTINGDATA) {
		LPGLYDEDEVICESETTINGDATA lpDeviceSettingData = p->lpBuffer;
		cJSON_AddNumberToObject(jsonObj, "CustomerID",
				lpDeviceSettingData->dwCustomerID);
		cJSON_AddNumberToObject(jsonObj, "StoreID",
				lpDeviceSettingData->dwStoreID);
		cJSON_AddNumberToObject(jsonObj, "MachineID",
				lpDeviceSettingData->dwMachineID);
		//cJSON_AddStringToObject(jsonObj, "IFType", (char)(lpDeviceSettingData->bIFType));

		//char ManualDepositName[7][20];
		//printf("nombre %s \n", lpDeviceSettingData->ManualDepositName[1]);
		//for (i = 0; i < GLY_DEPOSIT_MANUAL_NUM; i++) {
		//for (i = 0; i < GLY_DEPOSIT_MANUAL_NUM - 1; i++) {
		//printf(lpDeviceSettingData->ManualDepositName[i]);
		//memcpy(ManualDepositName[i], lpDeviceSettingData->ManualDepositName[i], sizeof(lpDeviceSettingData->ManualDepositName[i]));
		//}
		//cJSON_AddStringToObject(jsonObj, "DepositName", lpDeviceSettingData->ManualDepositName);
	} else if (p->ApiID == GLY_DE_DENOMINATIONDATA) {
		LPGLYDENOMINATION lpDenomination = p->lpBuffer;
		LPGLYCURRENCY currencies = lpDenomination->lpCurrencies;
		cJSON* denomination = createDenominationArray(p->Handle, currencies,
				lpDenomination->ulArraySize);
		cJSON_AddItemToObject(jsonObj, "Denomination", denomination);
	} else if (p->ApiID == GLY_DEPOSIT_COUNTRESULT
			|| p->ApiID == GLY_DEPOSIT_COLLECT
			|| p->ApiID == GLY_DEPOSIT_CLOSING) {
		LPGLYDEPOSITCOUNTER_EX counter = p->lpBuffer;

		cJSON_AddNumberToObject(jsonObj, "EventNumber", counter->EventNumber);
		cJSON_AddNumberToObject(jsonObj, "SequentialNo", counter->dwSquentialNo);
		cJSON_AddStringToObject(jsonObj, "UserID", counter->chUserID);

		LPGLYCOUNTER counters = counter->DepositData.lpCounters;
		cJSON_AddStringToObject(jsonObj, "Counter_ID", getString(p->Handle, counters->dwID));
		cJSON_AddStringToObject(jsonObj, "Counter_Status", getString(p->Handle, counters->dwStatus));

		GLYDENOMINATION lpDenomination = counters->Denomination;
		LPGLYCURRENCY currencies = lpDenomination.lpCurrencies;
		cJSON* denomination = createDenominationArray(p->Handle, currencies, lpDenomination.ulArraySize);
		cJSON_AddItemToObject(jsonObj, "Denomination", denomination);
		//Hora de la transaccion

		cJSON *timeObj;

		timeObj = cJSON_CreateObject();

		cJSON_AddNumberToObject(timeObj, "Year", counter->Time.tm_year + 1900);
		cJSON_AddNumberToObject(timeObj, "Month", counter->Time.tm_mon);
		cJSON_AddNumberToObject(timeObj, "Day", counter->Time.tm_mday);
		cJSON_AddNumberToObject(timeObj, "Hour", counter->Time.tm_hour);
		cJSON_AddNumberToObject(timeObj, "Minute", counter->Time.tm_min);
		cJSON_AddNumberToObject(timeObj, "Second", counter->Time.tm_sec);

		cJSON_AddItemToObject(jsonObj, "FechaDeposito", timeObj);

	} else if (p->ApiID == GLY_DEPOSIT_MANUALCOUNT){
		LPGLYDEPOSITMANUALCOUNTER_EX counter = p->lpBuffer;
		cJSON_AddNumberToObject(jsonObj, "EventNumber", counter->EventNumber);
		cJSON_AddNumberToObject(jsonObj, "SequentialNo", counter->dwSquentialNo);
		cJSON_AddStringToObject(jsonObj, "UserID", counter->chUserID);

		LPGLYAMOUNT counters = counter->DepositData.lpAmounts;
		cJSON_AddNumberToObject(jsonObj, "value",  counters->ulInteger);
		cJSON_AddNumberToObject(jsonObj, "decimal", counters->ulDecimal);
		cJSON_AddStringToObject(jsonObj, "currency", counters->cCurrencyID);

		int size = counter->DepositData.ulArraySize;

		cJSON *denomination = cJSON_CreateArray();

		cJSON_AddItemToObject(jsonObj, "Denomination", denomination);
		/*
		GLYDENOMINATION lpDenomination = counters->Denomination;
		LPGLYCURRENCY currencies = lpDenomination.lpCurrencies;
		cJSON* denomination = createDenominationArray(p->Handle, currencies, lpDenomination.ulArraySize);
		cJSON_AddItemToObject(jsonObj, "Denomination", denomination);
		*/

		cJSON *timeObj;
		timeObj = cJSON_CreateObject();

		cJSON_AddNumberToObject(timeObj, "Year", counter->Time.tm_year + 1900);
		cJSON_AddNumberToObject(timeObj, "Month", counter->Time.tm_mon);
		cJSON_AddNumberToObject(timeObj, "Day", counter->Time.tm_mday);
		cJSON_AddNumberToObject(timeObj, "Hour", counter->Time.tm_hour);
		cJSON_AddNumberToObject(timeObj, "Minute", counter->Time.tm_min);
		cJSON_AddNumberToObject(timeObj, "Second", counter->Time.tm_sec);

		cJSON_AddItemToObject(jsonObj, "FechaDeposito", timeObj);
	} else if (p->ApiID == GLY_DEPOSIT_ERROR){
		LPGLYDEPOSITERROR counter = p->lpBuffer;
		cJSON_AddNumberToObject(jsonObj, "EventNumber", counter->EventNumber);
		cJSON_AddNumberToObject(jsonObj, "DEErrorCode", counter->dwErrorCode);
		cJSON_AddNumberToObject(jsonObj, "DEErrorType", counter->dwErrorType);

		cJSON *timeObj;
		timeObj = cJSON_CreateObject();

		cJSON_AddNumberToObject(timeObj, "Year", counter->Time.tm_year + 1900);
		cJSON_AddNumberToObject(timeObj, "Month", counter->Time.tm_mon);
		cJSON_AddNumberToObject(timeObj, "Day", counter->Time.tm_mday);
		cJSON_AddNumberToObject(timeObj, "Hour", counter->Time.tm_hour);
		cJSON_AddNumberToObject(timeObj, "Minute", counter->Time.tm_min);
		cJSON_AddNumberToObject(timeObj, "Second", counter->Time.tm_sec);

		cJSON_AddItemToObject(jsonObj, "Fecha", timeObj);
	} else if (p->ApiID == GLY_DEPOSIT_CLEARERROR){
		LPGLYDEPOSITCLEARERROR_EX counter = p->lpBuffer;
		cJSON_AddNumberToObject(jsonObj, "EventNumber", counter->EventNumber);
		cJSON_AddNumberToObject(jsonObj, "DEErrorCode", counter->dwErrorCode);
		cJSON_AddNumberToObject(jsonObj, "DEErrorType", counter->dwErrorType);

		cJSON *timeObj;
		timeObj = cJSON_CreateObject();

		cJSON_AddNumberToObject(timeObj, "Year", counter->Time.tm_year + 1900);
		cJSON_AddNumberToObject(timeObj, "Month", counter->Time.tm_mon);
		cJSON_AddNumberToObject(timeObj, "Day", counter->Time.tm_mday);
		cJSON_AddNumberToObject(timeObj, "Hour", counter->Time.tm_hour);
		cJSON_AddNumberToObject(timeObj, "Minute", counter->Time.tm_min);
		cJSON_AddNumberToObject(timeObj, "Second", counter->Time.tm_sec);

		cJSON_AddItemToObject(jsonObj, "Fecha", timeObj);
	}

	JNIEnv* g_env = JNIEnvStart();

	//jstring jStringParam = (*g_env)->NewStringUTF(g_env, "hello from C");
	assert(g_env);
	assert(g_obj);
	assert(jsonObj);
	assert(g_mid);

	jstring jsonText = (*g_env)->NewStringUTF(g_env, cJSON_PrintUnformatted(jsonObj));

	(*g_env)->CallVoidMethod(g_env, g_obj, g_mid, jsonText);

	//
	JNIEnvEnd(g_env);

	GLY_FreeMemory((LPVOID) p);

	fflush(stdout);
}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1Open(
		JNIEnv *env, jobject thisObj, jstring name) {
	addlog("Opening Connection \n");

	GLYHANDLE* h;
	int result = 0;
	const char *nativeString = (*env)->GetStringUTFChars(env, name, 0);

	//result = GLY_GsiOpen(nativeString,&h, callback);
	result = GLY_Open(nativeString, &h);
	//return result;
	if (result == 0) {
		return h;
	} else {
		return 0;
	}
}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1SetCallBack(
		JNIEnv * env, jobject obj, jint handle, jobject classref) {

	g_obj = (*env)->NewGlobalRef(env, obj);

// save refs for callback
	jclass g_clazz = (*env)->GetObjectClass(env, g_obj);
	if (g_clazz == NULL) {
		printf("Failed to find class");
	}

	g_mid = (*env)->GetMethodID(env, g_clazz, "Callback",
			"(Ljava/lang/String;)V");
	if (g_mid == NULL) {
		printf("Unable to get method ref callback\n");
	}

	/*


	 jstring result = (jstring) (*jnienv)->CallObjectMethod(jnienv, classref, mid,(*jnienv)->NewStringUTF(jnienv, "hello jni"));
	 const char * nativeresult = (*jnienv)->GetStringUTFChars(jnienv, result, 0);
	 addlog("Echo from Setcallback: %s", nativeresult);

	 (*jnienv)->ReleaseStringUTFChars(jnienv, result, nativeresult);
	 */

	char* string;
	int result = 0;

	GLYHANDLE* h = handle;

	GLYCALLBACK *cb = callbackfn;

	result = GLY_SetCallBack(h, callbackfn);
	GLY_GetStringFromID(handle, result, &string);
	if (result == 0) {
		addlog("callback set ");
	} else {
		printf("callback error %s", string);
	}
	return result;
	/*
	 int* status;
	 LPGLYMACHINE machine;

	 GLY_DeGetStatus(h, machine);
	 GLY_GetStatus(h,&status);

	 while (status == GLY_STATUS_DLL_INITIALIZE_BUSY){
	 GLY_GetStatus(h,&status);
	 }

	 GLY_GetStringFromID(h,machine->dwMachine_Status, &string);
	 addlog("DEstatus %s", string);
	 GLY_GetStringFromID(h,status, &string);
	 addlog("status %s", string);

	 char* f;

	 return &h;
	 */
}

JNIEXPORT jstring JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1GetStatus(
		JNIEnv *env, jobject thisObj, jint handle) {
	char* string;
	int status;
	GLY_GetStatus(handle, &status);
	GLY_GetStringFromID(handle, status, &string);
	return (*env)->NewStringUTF(env, string);
}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1Close(
		JNIEnv *env, jobject thisObj, jint handle) {
	return GLY_Close(handle);
}

JNIEXPORT void JNICALL Java_com_mstn_DE_api_CapturaEventos_DE_1Start(
		JNIEnv *env, jobject obj) {

}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1DepositFixEvent(
		JNIEnv *env, jobject obj, jint handle, jint event) {
	return GLY_DepositFixEvent(handle, event);
}

JNIEXPORT jstring JNICALL Java_com_mstn_DE_api_CapturaEventos_DE_1GetText(
		JNIEnv *env, jobject obj, jint handle, jint id) {
	char* text = getString(handle, id);
	jstring jtext = (*env)->NewStringUTF(env, text);
	return jtext;
}

JNIEXPORT JNIEXPORT jstring JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1GetCounter(
		JNIEnv *env, jobject obj, jint handle) {

	//cJSON *jsonObj;
	//jsonObj = cJSON_CreateObject();

	LPGLYCOUNTERS * lpCounters;
	int response = GLY_GetCounter(handle, lpCounters);
	printf("Response %s \n", getString(handle, response));fflush(stdout);
	if (response == 1) {
		//LPGLYCOUNTERS counters = (*lpCounters);
		int i;
		printf("here==============\n");fflush(stdout);
		//printf("%d\n", (*lpCounters).ulArraySize);fflush(stdout);
		printf("here==============\n");fflush(stdout);
		/*
		 for (i = 0; i < (*counters)->ulArraySize; i++) {

		 GLYCOUNTER counter = (*counters)->lpCounters[i];
		 cJSON_AddStringToObject(jsonObj, "Counter_ID",
		 getString(handle, counter.dwID));
		 cJSON_AddStringToObject(jsonObj, "Counter_Status",
		 getString(handle, counter.dwStatus));

		 GLYDENOMINATION lpDenomination = counter.Denomination;
		 LPGLYCURRENCY currencies = lpDenomination.lpCurrencies;
		 cJSON* denomination = createDenominationArray(handle, currencies,
		 lpDenomination.ulArraySize);
		 cJSON_AddItemToObject(jsonObj, "Denomination", denomination);
		 }
		 */
	}
	//char* text = cJSON_PrintUnformatted(jsonObj);
	jstring jsonText = (*env)->NewStringUTF(env, "");
	return jsonText;
}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1UploadFirmware(JNIEnv *env, jobject jobj, jint handle){
	int result = GLY_Download(handle, "download.xml", "TEST1",5);
	return result;
}

JNIEXPORT jint JNICALL Java_com_mstn_DE_api_CapturaEventos_GLY_1DownloadFirmware(JNIEnv *env, jobject jobj, jint handle){
	const char* namelist[] = { "DEVICE_SETTING", NULL};
	void* logname = namelist ;

	const char *name[] = {
	      "DEVICE_SETTING"
	   };
	//const char *names = name;

	int result = GLY_LogRead(handle, logname ,5, NULL);
	return result;
}
