
package _do.com.bhdleon.integrationservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_RepeatingParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_RepeatingParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RepeatingStructure" type="{http://bhdleon.com.do/IntegrationServices}t_RepeatingStructure" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_RepeatingParameters", propOrder = {
    "repeatingStructure"
})
public class TRepeatingParameters {

    @XmlElement(name = "RepeatingStructure", required = true)
    protected List<TRepeatingStructure> repeatingStructure;

    /**
     * Gets the value of the repeatingStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the repeatingStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRepeatingStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TRepeatingStructure }
     * 
     * 
     */
    public List<TRepeatingStructure> getRepeatingStructure() {
        if (repeatingStructure == null) {
            repeatingStructure = new ArrayList<TRepeatingStructure>();
        }
        return this.repeatingStructure;
    }

}
