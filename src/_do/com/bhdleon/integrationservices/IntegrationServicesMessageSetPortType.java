
package _do.com.bhdleon.integrationservices;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IntegrationServicesMessageSetPortType", targetNamespace = "http://bhdleon.com.do/IntegrationServices")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IntegrationServicesMessageSetPortType {


    /**
     * 
     * @param integrationMessageRequest
     * @return
     *     returns _do.com.bhdleon.integrationservices.TIntegrationMessageResponse
     * @throws InvokeIntegrationServiceFault1
     */
    @WebMethod(operationName = "InvokeIntegrationService")
    @WebResult(name = "IntegrationMessageResponse", targetNamespace = "http://bhdleon.com.do/IntegrationServices", partName = "IntegrationMessageResponse")
    public TIntegrationMessageResponse invokeIntegrationService(
        @WebParam(name = "IntegrationMessageRequest", targetNamespace = "http://bhdleon.com.do/IntegrationServices", partName = "IntegrationMessageRequest")
        TIntegrationMessageRequest integrationMessageRequest)
        throws InvokeIntegrationServiceFault1
    ;

}
