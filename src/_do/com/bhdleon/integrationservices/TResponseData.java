
package _do.com.bhdleon.integrationservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_ResponseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_ResponseData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ErrorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ErrorText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResponseParameters" type="{http://bhdleon.com.do/IntegrationServices}t_ResponseParameters" minOccurs="0"/>
 *         &lt;element name="OutRepeatingParameters" type="{http://bhdleon.com.do/IntegrationServices}t_OutRepeatingParameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_ResponseData", propOrder = {
    "responseDate",
    "responseTime",
    "errorCode",
    "errorText",
    "responseParameters",
    "outRepeatingParameters"
})
public class TResponseData {

    @XmlElement(name = "ResponseDate", required = true)
    protected String responseDate;
    @XmlElement(name = "ResponseTime", required = true)
    protected String responseTime;
    @XmlElement(name = "ErrorCode", required = true)
    protected String errorCode;
    @XmlElement(name = "ErrorText", required = true)
    protected String errorText;
    @XmlElement(name = "ResponseParameters")
    protected TResponseParameters responseParameters;
    @XmlElement(name = "OutRepeatingParameters")
    protected TOutRepeatingParameters outRepeatingParameters;

    /**
     * Gets the value of the responseDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseDate() {
        return responseDate;
    }

    /**
     * Sets the value of the responseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseDate(String value) {
        this.responseDate = value;
    }

    /**
     * Gets the value of the responseTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseTime() {
        return responseTime;
    }

    /**
     * Sets the value of the responseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseTime(String value) {
        this.responseTime = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorText() {
        return errorText;
    }

    /**
     * Sets the value of the errorText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorText(String value) {
        this.errorText = value;
    }

    /**
     * Gets the value of the responseParameters property.
     * 
     * @return
     *     possible object is
     *     {@link TResponseParameters }
     *     
     */
    public TResponseParameters getResponseParameters() {
        return responseParameters;
    }

    /**
     * Sets the value of the responseParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link TResponseParameters }
     *     
     */
    public void setResponseParameters(TResponseParameters value) {
        this.responseParameters = value;
    }

    /**
     * Gets the value of the outRepeatingParameters property.
     * 
     * @return
     *     possible object is
     *     {@link TOutRepeatingParameters }
     *     
     */
    public TOutRepeatingParameters getOutRepeatingParameters() {
        return outRepeatingParameters;
    }

    /**
     * Sets the value of the outRepeatingParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link TOutRepeatingParameters }
     *     
     */
    public void setOutRepeatingParameters(TOutRepeatingParameters value) {
        this.outRepeatingParameters = value;
    }

}
