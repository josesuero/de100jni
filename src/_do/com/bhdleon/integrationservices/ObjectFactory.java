
package _do.com.bhdleon.integrationservices;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the _do.com.bhdleon.integrationservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IntegrationMessageRequest_QNAME = new QName("http://bhdleon.com.do/IntegrationServices", "IntegrationMessageRequest");
    private final static QName _IntegrationMessageResponse_QNAME = new QName("http://bhdleon.com.do/IntegrationServices", "IntegrationMessageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: _do.com.bhdleon.integrationservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TIntegrationMessageRequest }
     * 
     */
    public TIntegrationMessageRequest createTIntegrationMessageRequest() {
        return new TIntegrationMessageRequest();
    }

    /**
     * Create an instance of {@link TIntegrationMessageResponse }
     * 
     */
    public TIntegrationMessageResponse createTIntegrationMessageResponse() {
        return new TIntegrationMessageResponse();
    }

    /**
     * Create an instance of {@link TRepeatingStructure }
     * 
     */
    public TRepeatingStructure createTRepeatingStructure() {
        return new TRepeatingStructure();
    }

    /**
     * Create an instance of {@link TInternalParameters }
     * 
     */
    public TInternalParameters createTInternalParameters() {
        return new TInternalParameters();
    }

    /**
     * Create an instance of {@link TOutParameter }
     * 
     */
    public TOutParameter createTOutParameter() {
        return new TOutParameter();
    }

    /**
     * Create an instance of {@link TResponseParameters }
     * 
     */
    public TResponseParameters createTResponseParameters() {
        return new TResponseParameters();
    }

    /**
     * Create an instance of {@link TParameter }
     * 
     */
    public TParameter createTParameter() {
        return new TParameter();
    }

    /**
     * Create an instance of {@link TOutRepeatingParameters }
     * 
     */
    public TOutRepeatingParameters createTOutRepeatingParameters() {
        return new TOutRepeatingParameters();
    }

    /**
     * Create an instance of {@link TOutRepeatingStructure }
     * 
     */
    public TOutRepeatingStructure createTOutRepeatingStructure() {
        return new TOutRepeatingStructure();
    }

    /**
     * Create an instance of {@link TParameters }
     * 
     */
    public TParameters createTParameters() {
        return new TParameters();
    }

    /**
     * Create an instance of {@link TRequestData }
     * 
     */
    public TRequestData createTRequestData() {
        return new TRequestData();
    }

    /**
     * Create an instance of {@link TRepeatingParameters }
     * 
     */
    public TRepeatingParameters createTRepeatingParameters() {
        return new TRepeatingParameters();
    }

    /**
     * Create an instance of {@link TResponseData }
     * 
     */
    public TResponseData createTResponseData() {
        return new TResponseData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TIntegrationMessageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bhdleon.com.do/IntegrationServices", name = "IntegrationMessageRequest")
    public JAXBElement<TIntegrationMessageRequest> createIntegrationMessageRequest(TIntegrationMessageRequest value) {
        return new JAXBElement<TIntegrationMessageRequest>(_IntegrationMessageRequest_QNAME, TIntegrationMessageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TIntegrationMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://bhdleon.com.do/IntegrationServices", name = "IntegrationMessageResponse")
    public JAXBElement<TIntegrationMessageResponse> createIntegrationMessageResponse(TIntegrationMessageResponse value) {
        return new JAXBElement<TIntegrationMessageResponse>(_IntegrationMessageResponse_QNAME, TIntegrationMessageResponse.class, null, value);
    }

}
