
package _do.com.bhdleon.integrationservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_IntegrationMessageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_IntegrationMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestUser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestChannel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Parameters" type="{http://bhdleon.com.do/IntegrationServices}t_Parameters"/>
 *         &lt;element name="RepeatingParameters" type="{http://bhdleon.com.do/IntegrationServices}t_RepeatingParameters" minOccurs="0"/>
 *         &lt;element name="InternalParameters" type="{http://bhdleon.com.do/IntegrationServices}t_InternalParameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_IntegrationMessageRequest", propOrder = {
    "requestId",
    "requestDate",
    "requestTime",
    "requestUser",
    "requestChannel",
    "requestOperation",
    "parameters",
    "repeatingParameters",
    "internalParameters"
})
public class TIntegrationMessageRequest {

    @XmlElement(name = "RequestId", required = true)
    protected String requestId;
    @XmlElement(name = "RequestDate", required = true)
    protected String requestDate;
    @XmlElement(name = "RequestTime", required = true)
    protected String requestTime;
    @XmlElement(name = "RequestUser", required = true)
    protected String requestUser;
    @XmlElement(name = "RequestChannel", required = true)
    protected String requestChannel;
    @XmlElement(name = "RequestOperation", required = true)
    protected String requestOperation;
    @XmlElement(name = "Parameters", required = true)
    protected TParameters parameters;
    @XmlElement(name = "RepeatingParameters")
    protected TRepeatingParameters repeatingParameters;
    @XmlElement(name = "InternalParameters")
    protected TInternalParameters internalParameters;

    /**
     * Gets the value of the requestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets the value of the requestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Gets the value of the requestDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestDate() {
        return requestDate;
    }

    /**
     * Sets the value of the requestDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestDate(String value) {
        this.requestDate = value;
    }

    /**
     * Gets the value of the requestTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestTime() {
        return requestTime;
    }

    /**
     * Sets the value of the requestTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestTime(String value) {
        this.requestTime = value;
    }

    /**
     * Gets the value of the requestUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUser() {
        return requestUser;
    }

    /**
     * Sets the value of the requestUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUser(String value) {
        this.requestUser = value;
    }

    /**
     * Gets the value of the requestChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestChannel() {
        return requestChannel;
    }

    /**
     * Sets the value of the requestChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestChannel(String value) {
        this.requestChannel = value;
    }

    /**
     * Gets the value of the requestOperation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestOperation() {
        return requestOperation;
    }

    /**
     * Sets the value of the requestOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestOperation(String value) {
        this.requestOperation = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link TParameters }
     *     
     */
    public TParameters getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link TParameters }
     *     
     */
    public void setParameters(TParameters value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the repeatingParameters property.
     * 
     * @return
     *     possible object is
     *     {@link TRepeatingParameters }
     *     
     */
    public TRepeatingParameters getRepeatingParameters() {
        return repeatingParameters;
    }

    /**
     * Sets the value of the repeatingParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRepeatingParameters }
     *     
     */
    public void setRepeatingParameters(TRepeatingParameters value) {
        this.repeatingParameters = value;
    }

    /**
     * Gets the value of the internalParameters property.
     * 
     * @return
     *     possible object is
     *     {@link TInternalParameters }
     *     
     */
    public TInternalParameters getInternalParameters() {
        return internalParameters;
    }

    /**
     * Sets the value of the internalParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link TInternalParameters }
     *     
     */
    public void setInternalParameters(TInternalParameters value) {
        this.internalParameters = value;
    }

}
