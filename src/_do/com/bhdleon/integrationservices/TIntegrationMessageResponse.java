
package _do.com.bhdleon.integrationservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_IntegrationMessageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_IntegrationMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestData" type="{http://bhdleon.com.do/IntegrationServices}t_RequestData"/>
 *         &lt;element name="ResponseData" type="{http://bhdleon.com.do/IntegrationServices}t_ResponseData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_IntegrationMessageResponse", propOrder = {
    "requestData",
    "responseData"
})
public class TIntegrationMessageResponse {

    @XmlElement(name = "RequestData", required = true)
    protected TRequestData requestData;
    @XmlElement(name = "ResponseData", required = true)
    protected TResponseData responseData;

    /**
     * Gets the value of the requestData property.
     * 
     * @return
     *     possible object is
     *     {@link TRequestData }
     *     
     */
    public TRequestData getRequestData() {
        return requestData;
    }

    /**
     * Sets the value of the requestData property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRequestData }
     *     
     */
    public void setRequestData(TRequestData value) {
        this.requestData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link TResponseData }
     *     
     */
    public TResponseData getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link TResponseData }
     *     
     */
    public void setResponseData(TResponseData value) {
        this.responseData = value;
    }

}
