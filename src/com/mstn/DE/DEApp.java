package com.mstn.DE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.mstn.DE.api.DEApiWorker;
import com.mstn.DE.data.DEDataWorker;
import com.mstn.DE.sync.DEMQWorker;
import com.mstn.DE.sync.DESyncWorker;
import com.mstn.DE.workers.DEWorker;

public class DEApp {
	
	private static final Logger logger = LogManager.getLogger("DE100");
	private static String propFileName = "conf/config.properties";
	
	private static Properties getPropertyFile() throws IOException{
		if (!Files.exists(Paths.get(propFileName))){
			propFileName = "../" + propFileName;
		}
		InputStream inputStream = null;
		Properties prop = null;
		try {
			prop = new Properties();
			inputStream = new FileInputStream(propFileName);			 
			prop.load(inputStream);
		} catch (Exception e){
			String error = "El archivo de configuracion '" + propFileName + "' no fue encontrado";
			DEApp.log().error(error);
			throw new FileNotFoundException(error);			
		} finally {
			inputStream.close();

		}
		return prop;
	}
	
	public static String getConfig(String property) throws IOException {
		Properties prop = getPropertyFile();
		Object value = prop.get(property);
		if (value == null){
			DEApp.log().error("Propiedad {} No entrada en configuracion", property);
		}
		return value.toString();
	}
	
	public static boolean setConfig(String property, String value) throws IOException{
		Properties prop = getPropertyFile();
		prop.setProperty(property, value);
		FileOutputStream output = new FileOutputStream(propFileName);
		prop.store(output, "");
		return true;
	}

	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public static String genTimeStamp(Date now, String format) {
	    SimpleDateFormat sdfDate = new SimpleDateFormat(format);
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public static Integer genRequestID(JSONObject obj){
		return obj.getInt("ID");
	}

	public static String genUserId(String userID){
		return Integer.valueOf(userID.substring(0,userID.length() - 1)).toString();
	}
	
	public static byte[] genMQKey(String machineId){
		byte[] bytes = new byte[32];
		bytes = new StringBuilder("BOBEDAELECTRONICAS")
				.append(machineId)
				.toString()
				.getBytes();
	    return bytes;
	}

	ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues;
	
	public static Logger log(){
		return logger;
	}

	public DEApp() throws Exception {
		//new com.mstn.DE.data.DESql().nonquery("drop table if exists LOG");
		
		int queueSize = 20;
		queues = new ConcurrentHashMap<>();
		queues.put("data", new ArrayBlockingQueue<JSONObject>(queueSize));
		queues.put("sync", new ArrayBlockingQueue<JSONObject>(queueSize));
		queues.put("api", new ArrayBlockingQueue<JSONObject>(queueSize));
		queues.put("mq", new ArrayBlockingQueue<JSONObject>(queueSize));
		
		//DEExecutor exec = new DEExecutor();

		ExecutorService exec = Executors.newCachedThreadPool();
		
		List<Callable<DEWorker>> workers = new ArrayList<Callable<DEWorker>>();
		workers.add(new DEDataWorker(queues));
		workers.add(new DESyncWorker(queues));
		workers.add(new DEApiWorker(queues));
		workers.add(new DEMQWorker(queues));

		exec.invokeAll(workers);
		exec.shutdownNow();
		if (!exec.awaitTermination(100, TimeUnit.MICROSECONDS)) {
			DEApp.log().info("Esperando para terminar aplicacion");
			System.exit(0);
		}
		DEApp.log().info("Saliendo normalmente");

	}
}