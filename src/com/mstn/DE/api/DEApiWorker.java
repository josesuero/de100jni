package com.mstn.DE.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mstn.DE.DEApp;
import com.mstn.DE.DEException;
import com.mstn.DE.file.DEFile;
import com.mstn.DE.file.DEUser;
import com.mstn.DE.workers.DEWorker;

public class DEApiWorker extends DEWorker {

	ExecutorService apiDataService;
	JSONObject deviceSettingData;
	CapturaEventos apiData;

	ScheduledExecutorService verifyStatusService;

	class verifyStatusWorker extends Thread {
		ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues;

		public verifyStatusWorker(ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues) {
			this.queues = queues;
		}

		@Override
		public void run() {
			queues.get("captura").add(DEWorker.DECommand(DECommand.STATUS));
		}
	}

	public DEApiWorker(ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues) {
		super("api", queues);

		apiDataService = Executors.newSingleThreadExecutor();
		apiData = new CapturaEventos(queues.get("api"));
		this.queues.put("captura", apiData.getQueue());
		apiDataService.execute(apiData);

		// verifyStatusService = Executors.newSingleThreadScheduledExecutor();
		// verifyStatusWorker verifyStatus = new verifyStatusWorker(queues);
		// int time = 30;
		// verifyStatusService.scheduleAtFixedRate(verifyStatus, time, time,
		// TimeUnit.SECONDS);

	}

	private void endAll(JSONObject obj) {
		try {
			this.queues.get("data").add(obj);
			this.queues.get("sync").add(obj);
			this.queues.get("captura").add(obj);
			end = true;
			apiDataService.shutdown();
			if (!apiDataService.awaitTermination(60, TimeUnit.SECONDS)) {
				DEApp.log().info("Esperando para terminar coneccion");
				apiDataService.shutdownNow();
			}
		} catch (Exception e) {
			DEApp.log().error("Error ENDAll", e);
		}
	}

	public void doWork(JSONObject objOrg) {
		JSONObject obj = new JSONObject(objOrg, JSONObject.getNames(objOrg));
		DEApp.log().debug("Api working");
		DECommand comm = (DECommand) obj.get("DECommand");
		switch (comm) {
		case START:
			break;
		case EVENT:
			DEApp.log().debug("[EVENT] {}", obj.getInt("ApiID"));
			switch (obj.getInt("ApiID")) {
			case GlyCode.GLY_DE_DEVICESETTINGDATA:
				try {
					String id = new StringBuilder()
					.append(String.format("%04d", obj.getInt("CustomerID")))
					.append(String.format("%02d", obj.getInt("MachineID")))
					.toString();
					obj.put("MachineID", id);
					deviceSettingData = obj;
					// Verificar si cambio el DEID
					String deid = DEApp.getConfig("DEID");
					if (!deid.equals(id)) {
						obj.put("DECommand", DECommand.IDCHANGE);
						obj.put("LastBovedaID", deid);
						obj.put("NewBovedaID", id);
						obj.put("EventNumber", "0");
						obj.put("UserID", "000000000");
						queues.get("data").add(obj);
						// Generar Objeto de Data
						// Enviar a DATA
						DEApp.setConfig("DEID", id);
					}
				} catch (Exception e) {
					DEApp.log().error("Error cargando devicesetting", e);
				}
				break;
			case GlyCode.GLY_STATUS_CHANGE:
				String status = obj.getString("GLYSTATUS");
				DEApp.log().info("NUEVO STATUS: {}", status);
				break;
			case GlyCode.GLY_DE_DENOMINATIONDATA:
				break;
			case GlyCode.GLY_DEPOSIT_COUNTRESULT:
			case GlyCode.GLY_DEPOSIT_MANUALCOUNT:
				obj.put("DECommand", DECommand.DEPOSIT);
				processDepositEvent(obj);
				break;
			case GlyCode.GLY_DEPOSIT_COLLECT:
				obj.put("DECommand", DECommand.COLLECTION);
				processDepositEvent(obj);
				break;
			case GlyCode.GLY_DEPOSIT_CLOSING:
				obj.put("DECommand", DECommand.CLOSING);
				processDepositEvent(obj);
				break;
			case GlyCode.GLY_DEPOSIT_ERROR:
			case GlyCode.GLY_DEPOSIT_CLEARERROR:
				obj.put("DECommand", DECommand.ERROR);
				queues.get("captura").add(obj);
				break;				
			case GlyCode.GLY_ERROR_COMMUNICATION:
				try {
					DEApp.log().info("Reiniciando servicio de captura");
					queues.get("captura").add(DECommand(DECommand.END));

					apiDataService.shutdown();
					if (!apiDataService.awaitTermination(60, TimeUnit.SECONDS)) {
						DEApp.log().info("Esperando para terminar coneccion");
						apiDataService.shutdownNow();
					}

					apiDataService = Executors.newSingleThreadExecutor();
					apiData = new CapturaEventos(queues.get("api"));
					this.queues.put("captura", apiData.getQueue());
					apiDataService.execute(apiData);
				} catch (Exception e1) {
					DEApp.log().error("Error Reiniciando Captura", e1);
				}
			}
			break;
		case END:
			endAll(obj);
			break;
		case DEPOSIT:
		case COLLECTION:
		case CLOSING:
			// marcar el deposito en la maquina
			queues.get("captura").add(obj);
			break;
		case DATA:
			String value = apiData.callFunction((DEFunctions) obj.get("DEFunction"), 0);
			obj.put("DEStatus", value);
			obj.put("DECommand", DECommand.DATARESPONSE);

			queues.get(obj.getString("worker")).add(obj);
			break;
		case USERS:
			String status;
			String code;
			try {
				// if (true) throw new DEException("no pudo obtener
				// configuracion del equipo");
				JSONArray users = obj.getJSONArray("users");

				String date = new SimpleDateFormat("yyyyMMdd").format(new Date());

				String fileName = new StringBuilder().append(System.getProperty("user.dir")).append("\\..\\files\\")
						.append(date).append("_DEVICE_SETTING.log").toString();

				try {
					Files.delete(Paths.get(fileName));
				} catch (IOException e) {
					// file does not exists;
				}

				DEApp.log().info("Solicitando configuracion actual");
				String response = apiData.callFunction(DEFunctions.DOWNLOAD, 0);
				DEApp.log().info("Resultado archivo configuracion {}", response);

				if (response != null && response.equalsIgnoreCase("GLY_ERROR_COMMUNICATION"))
					throw new DEException("no pudo obtener configuracion del equipo");

				DEFile defile = new DEFile();
				defile.readSetFile(fileName.toString());
				defile.readUsers();
				DEApp.log().debug("Usuarios actuales");
				for (DEUser user : defile.getUsers()) {
					DEApp.log().debug("{} - {} ", user.getUserID(), user.getLevelText());
				}

				for (int i = 0; i < users.length(); i++) {
					DEUser user = (DEUser) users.get(i);
					if (user.getAction().equalsIgnoreCase("Crear")) {
						defile.userAdd(user);
					} else if (user.getAction().equalsIgnoreCase("Eliminar")) {
						defile.userDelete(user.getUserID());
					} else if (user.getAction().equalsIgnoreCase("Cambiar")) {
						defile.userUpdate(user);
					}
				}

				DEApp.log().debug("Usuarios Actualizados");
				for (DEUser user : defile.getUsers()) {
					DEApp.log().debug("{} - {} ", user.getUserID(), user.getLevelText());
				}

				// Files.delete(Paths.get(fileName));
				defile.fillUsers();
				defile.createFile();

				response = apiData.callFunction(DEFunctions.UPDATE, 0);
				if (response != null && response.equalsIgnoreCase("GLY_ERROR_COMMUNICATION"))
					throw new DEException("No subir configuracion al equipo");

				try {
					Files.delete(Paths.get("../files/newdata.set"));
				} catch (IOException e) {
					// file does not exists;
				}

				status = "MSGOK";
				code = " ";
				DEApp.log().info("Respuesta Actualizacion Usuarios:{} ", response);

			} catch (DEException e) {
				status = "ERR_NO_CONNECTION";
				code = e.getMessage();
			} catch (Exception e) {
				DEApp.log().error("Error API", e);
				status = "ERR_EXCEPTION";
				code = e.getMessage();
			}
			obj.put("DEStatus", status);
			obj.put("code", code);
			obj.put("DECommand", DECommand.DATARESPONSE);

			queues.get(obj.getString("worker")).add(obj);

			break;
		default:
			DEApp.log().debug("ApiWorker funcion {} no parametrizada", comm.toString());
			break;
		}
	}

	public void processDepositEvent(JSONObject obj) {

		// Generate reponse Object

		Date now = new Date();
		obj.put("FechaLectura", DEApp.genTimeStamp(now, "MM/dd/yyyy hh:mm:ss a"));

		obj.put("MachineID", deviceSettingData.getString("MachineID"));

		// Store Deposit
		queues.get("data").add(obj);

	}

}
