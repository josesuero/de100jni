package com.mstn.DE.api;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

import org.json.JSONObject;

import com.mstn.DE.DEApp;
import com.mstn.DE.workers.DEWorker;
import com.mstn.DE.workers.DEWorker.DECommand;
import com.mstn.DE.workers.DEWorker.DEFunctions;

public class CapturaEventos extends Thread {
	static {
		System.loadLibrary("DE100Jni");
	}
	
	int handle;
	protected boolean end = false;

	private BlockingQueue<JSONObject> queue;
	private BlockingQueue<JSONObject> apiQueue;

	private void addToQueue(JSONObject obj) {
		if (apiQueue != null) {
			apiQueue.add(obj);
		}
	}

	public CapturaEventos(BlockingQueue<JSONObject> queue) {
		this.apiQueue = queue;
		this.queue = new SynchronousQueue<JSONObject>();
	}

	public BlockingQueue<JSONObject> getQueue() {
		return queue;
	}

	native int GLY_Open(String name);

	native int GLY_SetCallBack(int handle, CapturaEventos jniclass);

	native String GLY_GetStatus(int handle);

	native int GLY_Close(int handle);

	native void DE_Start();

	native int GLY_DepositFixEvent(int handle, int event);

	native String DE_GetText(int handle, int event);
	
	native String GLY_GetCounter(int handle);
	
	native int GLY_UploadFirmware(int handle);
	
	native int GLY_DownloadFirmware(int handle);

 	public synchronized String callFunction(DEFunctions func, int event){
		switch(func){
			case STATUS:
				return GLY_GetStatus(handle);
			case FIXEVENT:
				Integer status;
				status = GLY_DepositFixEvent(handle, event);
				return status.toString();
			case COUNTERSTATUS:
				return GLY_GetCounter(handle);
			case UPDATE:
				return DE_GetText(handle, GLY_UploadFirmware(handle)) ;
			case DOWNLOAD:
				return DE_GetText(handle, GLY_DownloadFirmware(handle));
			default:
				return "0";
		}
		
	}
	
	public void Callback(String json) {

		JSONObject obj = new JSONObject(json);

		obj.put("DECommand", DECommand.EVENT);
		obj.put("GLYSTATUS", callFunction(DEFunctions.STATUS, 0));
		int apiID = obj.getInt("ApiID");
		DEApp.log().debug("callback activado {} ({})", DE_GetText(handle, apiID), apiID);

		if (!obj.getString("Event").equals("GLY_DOWNLOAD_PROGRESS")){
			addToQueue(obj);
		}
		
	}

	@Override
	public void run() {

		try {
			addToQueue(DEWorker.DECommand(DECommand.START));

			DE_Start();
			int result = 0;
			handle = GLY_Open("DLLTEST");

			if (handle != 0) {
				result = GLY_SetCallBack(handle, this);
				if (result == 0) {
					boolean timeout = false;

					int timeoutTime = 15;
					long startTime = System.nanoTime();

					while (callFunction(DEFunctions.STATUS, 0).equals("GLY_STATUS_DLL_INITIALIZE_BUSY") && !timeout) {
						if ((System.nanoTime() - startTime) / Math.pow(10, 9) > timeoutTime) {
							timeout = true;
						}
					}

					if (timeout) {
						DEApp.log().error("Timeout");
					} else {
						DEApp.log().info("inicializado");
						// GLY_GetStatus(handle));
						// boolean stop = false;
					}

					while (!end) {
						JSONObject obj = queue.take();
						doWork(obj);
						if (obj.has("DECommand") && obj.get("DECommand") == DECommand.END) {
							end = true;
						}
					}
					DEApp.log().info("finalizando captura");

					GLY_Close(handle);
				}
			}
		} catch (Exception e) {
			DEApp.log().error("Error ", e);
		}

	}

	public void doWork(JSONObject obj) {
		DEApp.log().debug("Captura woking");
		DECommand comm = (DECommand) obj.get("DECommand");
		switch (comm) {
		case START:
			break;
		case DEPOSIT:
		case COLLECTION:
		case CLOSING:
		case ERROR:
			int event = obj.getInt("EventNumber");
			int fixed = Integer.valueOf(callFunction(DEFunctions.FIXEVENT, event));//GLY_DepositFixEvent(handle, event);
			if (fixed == 0) {
				DEApp.log().info("marcando evento {}", event);
			} else {
				DEApp.log().error("error marcando event {} Status: {}", event, DE_GetText(handle, fixed));
				
				JSONObject commError = new JSONObject();
				commError.put("ApiID", GlyCode.GLY_ERROR_COMMUNICATION);
				commError.put("DECommand", DECommand.EVENT);
				commError.put("GLYSTATUS", callFunction(DEFunctions.STATUS, 0));
				apiQueue.add(commError);
				
			}
			break;
		case STATUS:
			Callback(new StringBuilder("{\"ApiID\":").append(GlyCode.GLY_STATUS_CHANGE).append("}").toString());
		default:
			DEApp.log().debug("Funcion {} no parametrizada", comm.toString());
			break;
		}
	}

	public int DownloadFirmware(){
		return GLY_DownloadFirmware(handle);
	}
	
	public int UploaddFirmware(){
		return GLY_UploadFirmware(handle);
	}
}
