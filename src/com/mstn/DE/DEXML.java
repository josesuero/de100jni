package com.mstn.DE;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import nu.xom.*;

public class DEXML {
	public static Document getXmlDocument(String xml) throws ValidityException, ParsingException, IOException{
		InputStream is = new ByteArrayInputStream( xml.getBytes() );
		Document document = new Builder().build(is);
		return document;
	}
	
	public static Node getXMLValue(Document document,String path){
		Nodes nodes = document.query(path);
		return nodes.get(0);
	}
	
	public static Nodes getNode(Document document, String path){
		return document.query(path);
	}
}
