package com.mstn.DE.sync;

import java.io.IOException;
import java.net.*;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.mstn.DE.DEApp;

import _do.com.bhdleon.integrationservices.IntegrationServices;
import _do.com.bhdleon.integrationservices.IntegrationServicesMessageSetPortType;

public class DESync {
	//public static final;

	public static boolean netIsAvailable() {
		try {
			String sendUrl = DEApp.getConfig("syncurl");
			final URL url = new URL(sendUrl);
			final URLConnection conn = url.openConnection();
			conn.setConnectTimeout(getTimeOut());
			conn.connect();
			return true;
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			return false;
		}
	}

	public static boolean sync() {
		// Extract Data to sync
		// verify conectivity

		if (netIsAvailable()) {
			// server available
			// send to server
			// verify sended
			// mark/delete records
		}
		return true;
	}

	public static int getTimeOut() {
		
		try {
			return Integer.parseInt(DEApp.getConfig("timeout")) * 1000;
		} catch (NumberFormatException | IOException e) {
			DEApp.log().error("Error SYNC",e);
		}
		//default value
		return 10000;
	}
	
	private IntegrationServicesMessageSetPortType ws;

	// You may want to synchronize on this method depending on your use case
	public IntegrationServicesMessageSetPortType getService() {
		try {
			if (this.ws == null) {
				// We create a thread so that we can set the ClassLoader
				Thread t = new Thread() {
					public void run() {
						synchronized (this) {
							// Get the string of your webservice WSDL from
							// somewhere
							URL srvUrl = null;

							try {
								String sendUrl = DEApp.getConfig("syncurl");
								String url = new StringBuilder(sendUrl).append("/IntegrationServices?wsdl").toString();
								srvUrl = new URL(url);
							} catch (IOException e) {
								DEApp.log().error("Error Sync",e);
							}

							QName qName = new QName("http://bhdleon.com.do/IntegrationServices", "IntegrationServices");
							Service service = Service.create(srvUrl, qName);
							
							/*
							Map<String, Object> context = ((BindingProvider)service).getRequestContext();
							//Set timeout params
							context.put("com.sun.xml.internal.ws.connect.timeout", getTimeOut());
							context.put("com.sun.xml.internal.ws.request.timeout", getTimeOut());
							context.put("com.sun.xml.ws.request.timeout", getTimeOut());
							context.put("com.sun.xml.ws.connect.timeout", getTimeOut());
							*/
							ws = service.getPort(IntegrationServicesMessageSetPortType.class);
							notify();
						}
					}
				};

				// Thread.currentThread().getContextClassloader()
				// returns null in
				// com.sun.xml.internal.ws.client.ClientContainer.
				// (See
				// http://hg.openjdk.java.net/jdk8/jdk8/jaxws/file/d03dd22762db/src/share/jaxws_classes/com/sun/xml/internal/ws/client/ClientContainer.java,
				// lines 39-47)
				// To work around that, I force setting of the
				// ContextClassLoader
				// on this thread (in which the Service.getPort() method will
				// run) so
				// that when ClientContainer calls
				// Thread.currentThread().getContextClassLoader(), it doesn't
				// get a null
				// (i.e., the bootstrap classloader).
				//
				t.setContextClassLoader(IntegrationServices.class.getClassLoader());
				t.start();
				// Wait until above thread completes in order to return
				// yourWebService
				synchronized (t) {
					try {
						t.wait();
					} catch (InterruptedException e) {
						DEApp.log().error("Error Sync", e);
					}
				}
			}

		} catch (Exception ex) {
			DEApp.log().error("Error Sync",ex);
		}
		return this.ws;
	}
}
