package com.mstn.DE.sync;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mstn.DE.DEApp;
import com.mstn.DE.api.GlyCode;
import com.mstn.DE.data.DESql;
import com.mstn.DE.workers.DEWorker;

import _do.com.bhdleon.integrationservices.IntegrationServicesMessageSetPortType;
import _do.com.bhdleon.integrationservices.InvokeIntegrationServiceFault1;
import _do.com.bhdleon.integrationservices.TIntegrationMessageRequest;
import _do.com.bhdleon.integrationservices.TIntegrationMessageResponse;
import _do.com.bhdleon.integrationservices.TOutParameter;
import _do.com.bhdleon.integrationservices.TParameter;
import _do.com.bhdleon.integrationservices.TParameters;
import _do.com.bhdleon.integrationservices.TRepeatingParameters;
import _do.com.bhdleon.integrationservices.TRepeatingStructure;
import _do.com.bhdleon.integrationservices.TResponseData;

public class DESyncWorker extends DEWorker {

	ScheduledExecutorService syncPastService;

	class SyncPastFailed extends Thread {

		@Override
		public void run() {

			try {
				JSONObject statusObj = new JSONObject();
				statusObj.put("DEFunction", DEFunctions.STATUS);
				statusObj.put("worker", "sync");
				statusObj.put("DECommand", DECommand.DATA);
				queues.get("api").add(statusObj);

				DESql desql = new DESql();
				StringBuilder sqlstr = new StringBuilder("select * from log where sync = 2")
						.append(" or (fechahora <= ")
						.append((new java.util.Date(new java.util.Date().getTime() - (5 * 60000))).getTime())
						.append(" and sync = 0)");
				JSONArray items = desql.query(sqlstr.toString());

				DEApp.log().debug("Iniciando servicio de sincronizacion");
				if (items.length() > 0) {
					DEApp.log().info("Elementos pendientes {}", items.length());
				}
				for (int i = 0; i < items.length(); i++) {
					JSONObject item = new JSONObject(items.getJSONObject(i).getString("DATA"));
					item.put("DECommand", DECommand.valueOf(item.getString("DECommand")));
					try {
						doWork(item);
					} catch (Exception ie) {
						DEApp.log().error("Sync Error procesando evento pendiente", ie);
					}
				}

				// Funcion de mantenimiento de datos
				// TODO PONER DETERMINAR MANTENIMIENTO

			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
			}
		}
	}

	DESync sync;

	public DESyncWorker(ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues) {
		super("sync", queues);

		syncPastService = Executors.newSingleThreadScheduledExecutor();
		SyncPastFailed genData = new SyncPastFailed();
		int time = 300;
		try {
			time = Integer.valueOf(DEApp.getConfig("RepeatSyncTime"));
		} catch (Exception e) {
			DEApp.log().error("Error Sync", e);
		}
		syncPastService.scheduleAtFixedRate(genData, 0, time, TimeUnit.SECONDS);
		sync = new DESync();
	}

	private TIntegrationMessageRequest fillHeader(TIntegrationMessageRequest data, JSONObject obj) {
		Date now = new Date();
		Integer requestID = DEApp.genRequestID(obj);
		data.setRequestId(requestID.toString());
		data.setRequestDate(DEApp.genTimeStamp(now, "MM/dd/yyyy hh:mm:ss a"));
		data.setRequestTime(DEApp.genTimeStamp(now, "hh:mm:ss a"));
		data.setRequestUser(DEApp.genUserId(obj.getString("UserID")));
		data.setRequestChannel("BOVEDA_ELECTRONICA");
		return data;
	}

	private TIntegrationMessageRequest collectionRequest(JSONObject obj, String type)
			throws ClassNotFoundException, SQLException {
		JSONArray denominations = obj.getJSONArray("Denomination");
		int requestID = DEApp.genRequestID(obj);

		TIntegrationMessageRequest data = new TIntegrationMessageRequest();

		data = fillHeader(data, obj);
		data.setRequestOperation("NotificacionEvento" + type);

		TParameters params = new TParameters();

		List<TParameter> parameters = params.getParameter();

		TParameter param = new TParameter();
		param.setDescription("EVENTO");
		param.setName("EVENTO");
		param.setValue(type);
		parameters.add(param);

		param = new TParameter();
		param.setDescription("USUARIO");
		param.setName("USUARIO");
		param.setValue(DEApp.genUserId(obj.getString("UserID")));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("IDBOVEDA");
		param.setName("ID_BOVEDA");
		param.setValue(obj.getString("MachineID"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("FECHALECTURA");
		param.setName("FECHALECTURA");
		param.setValue(obj.getString("FechaLectura"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("IDTRANSACCION");
		param.setName("IDTRANSACCION");
		param.setValue(DEApp.genRequestID(obj).toString());
		parameters.add(param);

		// String FechaDeposito = String.format("MM/dd/yyyy hh:mm:ss a", );
		JSONObject fechaDeposito = obj.getJSONObject("FechaDeposito");
		Calendar calendar = new GregorianCalendar(fechaDeposito.getInt("Year"), fechaDeposito.getInt("Month"),
				fechaDeposito.getInt("Day"), fechaDeposito.getInt("Hour"), fechaDeposito.getInt("Minute"),
				fechaDeposito.getInt("Second"));

		param = new TParameter();
		param.setDescription("FECHAEVENTO");
		param.setName("FECHAEVENTO");
		param.setValue(DEApp.genTimeStamp(calendar.getTime(), "MM/dd/yyyy hh:mm:ss a"));
		parameters.add(param);

		TRepeatingParameters repeatingParams = new TRepeatingParameters();
		List<TRepeatingStructure> repeatingStruct = repeatingParams.getRepeatingStructure();

		TRepeatingStructure struct;

		for (int i = 0; i < denominations.length(); i++) {
			JSONObject denomination = denominations.getJSONObject(i);
			Integer value = denomination.getInt("Value");
			Integer counts = denomination.getInt("Counts");
			String moneda = denomination.getString("CurrencyID");

			if (counts == 0) {
				continue;
			}

			struct = new TRepeatingStructure();
			struct.setDesription("DENOMINACION");

			param = new TParameter();
			param.setDescription("MONEDA");
			param.setName("MONEDA");
			param.setValue(moneda);
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("DENOMINACION");
			param.setName("DENOMINACION");
			param.setValue(value.toString());
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("CANTIDAD");
			param.setName("CANTIDAD");
			param.setValue(counts.toString());
			struct.getParameter().add(param);

			repeatingStruct.add(struct);
		}

		// Depositos

		DESql desql = new DESql();
		String sql = new StringBuilder("select * from log where ").append(type).append(" = ").append(requestID)
				.append(" and id_type = '").append(DECommand.DEPOSIT.toString()).append("'").toString();
		
		DEApp.log().debug("Query Depositos en {}: {}", type, sql);
		
		JSONArray deposits = desql.query(sql);
		DEApp.log().debug("Cantidad de depositos: {}", deposits.length());

		HashMap<String, Double> montoTotal = new HashMap<>();
		montoTotal.put("MONTOTOTALAUTOMATICODOP", 0.00);
		montoTotal.put("MONTOTOTALAUTOMATICOUSD", 0.00);
		montoTotal.put("MONTOTOTALAUTOMATICOEUR", 0.00);
		montoTotal.put("MONTOTOTALMANUALDOP", 0.00);
		montoTotal.put("MONTOTOTALMANUALEUR", 0.00);
		montoTotal.put("MONTOTOTALMANUALUSD", 0.00);

		for (int i = 0; i < deposits.length(); i++) {
			JSONObject deposit = deposits.getJSONObject(i);
			JSONObject depositObj = new JSONObject(deposit.getString("DATA"));

			struct = new TRepeatingStructure();
			struct.setDesription("DEPOSITO");

			param = new TParameter();
			param.setDescription("USUARIO");
			param.setName("USUARIO");
			param.setValue(DEApp.genUserId(depositObj.getString("UserID")));
			struct.getParameter().add(param);

			String monedaDeposito = "";
			Double montoDeposito = 0.00;
			JSONArray denominationList = depositObj.getJSONArray("Denomination");
			for (int x = 0; x < denominationList.length(); x++) {
				JSONObject denomination = denominationList.getJSONObject(x);
				Integer value = denomination.getInt("Value");
				Integer counts = denomination.getInt("Counts");
				if (denomination.getInt("Counts") > 0) {
					monedaDeposito = denomination.getString("CurrencyID");
				}
				montoDeposito += value * counts;
			}

			param = new TParameter();
			param.setDescription("IDTRANSACCION");
			param.setName("IDTRANSACCION");
			param.setValue(DEApp.genRequestID(depositObj).toString());
			struct.getParameter().add(param);

			String tipoDeposito;
			if (depositObj.getInt("ApiID") == GlyCode.GLY_DEPOSIT_COUNTRESULT) {
				tipoDeposito = "AUTOMATICO";
			} else {
				tipoDeposito = "MANUAL";
				montoDeposito = depositObj.getDouble("value") + (depositObj.getInt("decimal") / 100);
				monedaDeposito = depositObj.getString("currency");
			}

			param = new TParameter();
			param.setDescription("TIPODEPOSITO");
			param.setName("TIPODEPOSITO");
			param.setValue(tipoDeposito);
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("MONEDA");
			param.setName("MONEDA");
			param.setValue(monedaDeposito);
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("MONTO");
			param.setName("MONTO");
			param.setValue(montoDeposito.toString());
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("ERRORCODE");
			param.setName("ERRORCODE");
			param.setValue(deposit.getString("ERRORCODE"));
			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("NUMEROCOMPROBANTE");
			param.setName("NUMEROCOMPROBANTE");
			param.setValue(deposit.getString("COMPROBANTE"));
			struct.getParameter().add(param);

			if (!montoTotal.containsKey("MONTOTOTAL" + tipoDeposito + monedaDeposito)) {
				montoTotal.put("MONTOTOTAL" + tipoDeposito + monedaDeposito, 0.00);
			}
			Double monto = montoTotal.get("MONTOTOTAL" + tipoDeposito + monedaDeposito) + (montoDeposito);
			montoTotal.put("MONTOTOTAL" + tipoDeposito + monedaDeposito, monto);

			repeatingStruct.add(struct);
		}

		for (String key : montoTotal.keySet()) {
			String name = key;
			param = new TParameter();
			param.setDescription(name);
			param.setName(name);
			param.setValue(montoTotal.get(key).toString());
			parameters.add(param);
		}

		data.setParameters(params);
		data.setRepeatingParameters(repeatingParams);
		return data;
	}

	private TIntegrationMessageRequest depositRequest(JSONObject obj) {
		JSONArray denominations = obj.getJSONArray("Denomination");

		Integer requestID = DEApp.genRequestID(obj);

		TIntegrationMessageRequest data = new TIntegrationMessageRequest();
		data = fillHeader(data, obj);

		TParameters params = new TParameters();

		List<TParameter> parameters = params.getParameter();

		TParameter param = new TParameter();
		param.setDescription("EVENTO");
		param.setName("EVENTO");
		param.setValue("DEPOSITO");
		parameters.add(param);

		param = new TParameter();
		param.setDescription("USUARIO");
		param.setName("USUARIO");
		param.setValue(DEApp.genUserId(obj.getString("UserID")));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("IDTRANSACCION");
		param.setName("IDTRANSACCION");
		param.setValue(requestID.toString());
		parameters.add(param);

		param = new TParameter();
		param.setDescription("IDBOVEDA");
		param.setName("ID_BOVEDA");
		param.setValue(obj.getString("MachineID"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("FECHALECTURA");
		param.setName("FECHALECTURA");
		param.setValue(obj.getString("FechaLectura"));
		parameters.add(param);

		// String FechaDeposito = String.format("MM/dd/yyyy hh:mm:ss a", );
		JSONObject fechaDeposito = obj.getJSONObject("FechaDeposito");
		Calendar calendar = new GregorianCalendar(fechaDeposito.getInt("Year"), fechaDeposito.getInt("Month"),
				fechaDeposito.getInt("Day"), fechaDeposito.getInt("Hour"), fechaDeposito.getInt("Minute"),
				fechaDeposito.getInt("Second"));

		param = new TParameter();
		param.setDescription("FECHADEPOSITO");
		param.setName("FECHADEPOSITO");
		param.setValue(DEApp.genTimeStamp(calendar.getTime(), "MM/dd/yyyy hh:mm:ss a"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("FECHASINCRONIZACION");
		param.setName("FECHASINCRONIZACION");
		param.setValue(data.getRequestDate());
		parameters.add(param);

		TRepeatingParameters repeatingParams = new TRepeatingParameters();
		List<TRepeatingStructure> repeatingStruct = repeatingParams.getRepeatingStructure();

		TRepeatingStructure struct;
		Double montoTotal = 0.00;
		String moneda = "";

		for (int i = 0; i < denominations.length(); i++) {
			JSONObject denomination = denominations.getJSONObject(i);
			Integer value = denomination.getInt("Value");
			Integer counts = denomination.getInt("Counts");
			if (counts == 0) {
				continue;
			}
			moneda = denomination.getString("CurrencyID");
			montoTotal += value * counts;
			struct = new TRepeatingStructure();
			struct.setDesription("DENOMINACION");

			param = new TParameter();
			param.setDescription("DENOMINACION");
			param.setName("DENOMINACION");
			param.setValue(value.toString());

			struct.getParameter().add(param);

			param = new TParameter();
			param.setDescription("CANTIDAD");
			param.setName("CANTIDAD");
			param.setValue(counts.toString());

			struct.getParameter().add(param);
			repeatingStruct.add(struct);
		}

		String tipoDeposito;
		if (obj.getInt("ApiID") == GlyCode.GLY_DEPOSIT_COUNTRESULT) {
			tipoDeposito = "AUTOMATICO";
		} else {
			tipoDeposito = "MANUAL";
			montoTotal = obj.getDouble("value") + (obj.getInt("decimal") / 100);
			moneda = obj.getString("currency");
		}

		data.setRequestOperation("NotificacionDeposito" + tipoDeposito);

		param = new TParameter();
		param.setDescription("TIPODEPOSITO");
		param.setName("TIPODEPOSITO");
		param.setValue(tipoDeposito);
		parameters.add(param);

		param = new TParameter();
		param.setDescription("MONTOTOTAL");
		param.setName("MONTOTOTAL");
		param.setValue(montoTotal.toString());
		parameters.add(param);

		param = new TParameter();
		param.setDescription("MONEDA");
		param.setName("MONEDA");
		param.setValue(moneda);
		parameters.add(param);

		data.setParameters(params);
		data.setRepeatingParameters(repeatingParams);
		return data;
	}

	private TIntegrationMessageRequest idChangeRequest(JSONObject obj) throws ClassNotFoundException, SQLException {

		TIntegrationMessageRequest data = new TIntegrationMessageRequest();

		data = fillHeader(data, obj);
		data.setRequestOperation("ChangeBOVID");

		TParameters params = new TParameters();

		List<TParameter> parameters = params.getParameter();

		TParameter param = new TParameter();
		param.setDescription("LASTBOVEDAID");
		param.setName("LASTBOVEDAID");
		param.setValue(obj.getString("LastBovedaID"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("NEWBOVEDAID");
		param.setName("NEWBOVEDAID");
		param.setValue(DEApp.genUserId(obj.getString("NewBovedaID")));
		parameters.add(param);

		TRepeatingParameters repeatingParams = new TRepeatingParameters();

		data.setParameters(params);
		data.setRepeatingParameters(repeatingParams);
		return data;
	}

	private TIntegrationMessageRequest errorDataBase(JSONObject obj) throws ClassNotFoundException, SQLException {

		TIntegrationMessageRequest data = new TIntegrationMessageRequest();

		data = fillHeader(data, obj);
		data.setRequestOperation("ERRORBOVEDA");

		TParameters params = new TParameters();

		List<TParameter> parameters = params.getParameter();

		TParameter param = new TParameter();
		param.setDescription("TIPO");
		param.setName("TIPO");
		param.setValue(obj.getString("errortype"));
		parameters.add(param);

		param = new TParameter();
		param.setDescription("ERROR");
		param.setName("ERROR");
		param.setValue(obj.getString("error"));
		parameters.add(param);

		if (obj.has("cause")) {
			param = new TParameter();
			param.setDescription("CAUSE");
			param.setName("CAUSE");
			param.setValue(obj.getString("cause"));
			parameters.add(param);
		}

		if (obj.has("stack")) {
			param = new TParameter();
			param.setDescription("STACK");
			param.setName("STACK");
			param.setValue(obj.getString("stack"));
			parameters.add(param);
		}

		TRepeatingParameters repeatingParams = new TRepeatingParameters();

		data.setParameters(params);
		data.setRepeatingParameters(repeatingParams);
		return data;
	}

	private TResponseData sendIntegrationRequest(TIntegrationMessageRequest data, int ID)
			throws InvokeIntegrationServiceFault1, ClassNotFoundException, SQLException {

		if (DESync.netIsAvailable()) {
			IntegrationServicesMessageSetPortType ws = sync.getService();
			TIntegrationMessageResponse response;
			response = ws.invokeIntegrationService(data);

			TResponseData responseData = response.getResponseData();
			DEApp.log().debug(responseData.getErrorText());
			DEApp.log().debug(responseData.getErrorCode());

			DESql desql = new DESql();
			desql.nonquery(new StringBuilder("update log set sync = 3 where sequence = ").append(ID).toString());

			return responseData;
		} else {
			return null;
		}
	}

	public void doWork(JSONObject objOrg) {
		JSONObject obj = new JSONObject(objOrg, JSONObject.getNames(objOrg));

		DEApp.log().debug("working sync");
		DECommand command = (DECommand) obj.get("DECommand");
		DESql desql;
		switch (command) {
		case DEPOSIT:

			try {
				TResponseData result = sendIntegrationRequest(depositRequest(obj), obj.getInt("ID"));
				if (result != null) {
					DEApp.log().info("Sync successfull");
					obj.put("SYNCERRORCODE", result.getErrorCode());
					String comprobante = "";
					List<TOutParameter> outParamList = result.getResponseParameters().getOutParameter();
					if (outParamList.size() > 0) {
						comprobante = outParamList.get(0).getValue();
					}
					obj.put("SYNCCOMPROBANTE", comprobante);
					obj.put("DECommand", DECommand.DEPOSITDONE);
				} else {
					DEApp.log().error("Sync Error no network");
					obj.put("DECommand", DECommand.DEPOSITFAILED);
				}
			} catch (Exception e) {
				DEApp.log().error("Sync Error ", e);
				obj.put("DECommand", DECommand.DEPOSITFAILED);
			}
			queues.get("data").add(obj);
			break;
		case COLLECTION:
			// VERIFICAR DEPOSITOS PENDIENTES;
			try {
				desql = new DESql();
				String sql = new StringBuilder("select sequence from log where sync != 1 and collection = ")
						.append(DEApp.genRequestID(obj)).append(" and id_type = '").append(DECommand.DEPOSIT)
						.append("'").toString();
				JSONArray deposits = desql.query(sql);
				if (deposits.length() > 0) {
					DEApp.log().info("Depositos pendientes de syncronizar");
					obj.put("DECommand", DECommand.COLLECTIONFAILED);
				} else {
					TResponseData result = sendIntegrationRequest(collectionRequest(obj, "COLLECTION"),
							obj.getInt("ID"));
					if (result != null) {
						DEApp.log().info("Sync successfull");
						obj.put("SYNCERRORCODE", result.getErrorCode());
						obj.put("DECommand", DECommand.COLLECTIONDONE);
					} else {
						DEApp.log().info("Sync Error no network");
						obj.put("DECommand", DECommand.COLLECTIONFAILED);
					}
				}
			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
				obj.put("DECommand", DECommand.COLLECTIONFAILED);
			}
			queues.get("data").add(obj);
			break;
		case CLOSING:
			try {
				TResponseData result = sendIntegrationRequest(collectionRequest(obj, "CLOSING"), obj.getInt("ID"));
				if (result != null) {
					DEApp.log().info("Sync successfull");
					obj.put("SYNCERRORCODE", result.getErrorCode());
					obj.put("DECommand", DECommand.CLOSINGDONE);
				} else {
					DEApp.log().info("Sync Error no network");
					obj.put("DECommand", DECommand.CLOSINGFAILED);
				}
			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
				obj.put("DECommand", DECommand.CLOSINGFAILED);
			}
			queues.get("data").add(obj);
			break;
		case IDCHANGE:
			try {
				TResponseData result = sendIntegrationRequest(idChangeRequest(obj), obj.getInt("ID"));
				if (result != null) {
					DEApp.log().info("Sync successfull");
					obj.put("SYNCERRORCODE", result.getErrorCode());
					obj.put("DECommand", DECommand.IDCHANGEDONE);
				} else {
					DEApp.log().info("Sync Error no network");
					obj.put("DECommand", DECommand.IDCHANGEFAILED);
				}
			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
				obj.put("DECommand", DECommand.IDCHANGEFAILED);
			}
			queues.get("data").add(obj);
			break;
		case ERRORDATABASE:
			try {
				TResponseData result = sendIntegrationRequest(errorDataBase(obj), obj.getInt("ID"));
				if (result != null) {
					DEApp.log().info("Sync successfull");
				} else {
					DEApp.log().error("Sync Error no network");
				}
			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
			}
			break;
		case END:
			DEApp.log().info("Ending sync ");
			syncPastService.shutdown();
			break;
		case DATARESPONSE:
			try {
				DEApp.log().info("Status Actual {} ", obj.getString("DEStatus"));
			} catch (Exception e) {
				DEApp.log().error("Error Sync", e);
			}
			break;

		default:
			DEApp.log().debug("DESyncWorker funcion {} no parametrizada", command.toString());
			break;
		}
	}
}
