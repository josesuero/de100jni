package com.mstn.DE.sync;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.mstn.DE.DEApp;
import com.mstn.DE.DEXML;
import com.mstn.DE.file.DEUser;
import com.mstn.DE.workers.DEWorker;

import nu.xom.*;

public class DEMQWorker extends DEWorker {
	
	private static final DEMQ mq = new DEMQ();

	public DEMQWorker(ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues) {
		super("mq", queues);
		try {
			DEApp.log().info("Starting MQ Worker");
			mq.connect(DEApp.genMQKey(DEApp.getConfig("DEID")));
		} catch (MQException | JSONException | IOException e) {
			DEApp.log().error("Error MQ",e);
		}
		queues.get("mq").add(DEWorker.DECommand(DECommand.DATA));
	}
	

	@Override
	public void doWork(JSONObject obj) {
		DECommand command = (DECommand)obj.get("DECommand");
		MQMessage message = null;
		switch(command){
		case START:
			
			//queues.get("api").add(DEWorker.DECommand(DECommand.DATA).put("DEFunction", DEFunctions.COUNTERSTATUS).put("worker", "mq"));
			break;
		case DATA:
 			try {
				DEApp.log().info("Waiting for message");
				message = mq.getMessage();
				DEApp.log().info("Mensaje recibido");
				
				StringBuilder xmlStr = new StringBuilder();
				String line = null;
				do {
					 line = message.readLine();
					 xmlStr.append(line);
				} while (!line.equals(""));
				Document document = DEXML.getXmlDocument(xmlStr.toString());
				String mqOperation = DEXML.getXMLValue(document, "/BovedaElectronicaRequest/RequestOperation").getValue();
				
				JSONObject statusObj = new JSONObject();
				statusObj.put("worker", "mq");
				statusObj.put("xml", document);
				statusObj.put("message", message);
				if (mqOperation.equalsIgnoreCase("SolicitarInfoBoveda")){
					statusObj.put("DEFunction", DEFunctions.STATUS);
					statusObj.put("DECommand", DECommand.DATA);
					queues.get("api").add(statusObj);
				} else if (mqOperation.equalsIgnoreCase("ConsultaDBLocal")){
					statusObj.put("DEFunction", DEFunctions.DATA);
					statusObj.put("DECommand", DECommand.DATA);
					queues.get("data").add(statusObj);
				} else if (mqOperation.equalsIgnoreCase("AdministrarUsuarios")){
					statusObj.put("DECommand", DECommand.USERS);
					statusObj.put("DEFunction", DEFunctions.USERS);
					statusObj.put("xml", document);
					
					Nodes usuarios = DEXML.getNode(document, "/BovedaElectronicaRequest/Usuarios/Usuario");
					JSONArray userList = new JSONArray();
					for (int i = 0;i < usuarios.size();i++){
						Node usernode = usuarios.get(i);
						int level = 1;
						String levelstr = usernode.query("TIPOUSUARIO").get(0).getValue();
						
						if (levelstr.equalsIgnoreCase("DEPOSITANTE")){
							level = 1;
						} else if (levelstr.equalsIgnoreCase("SUPERVISOR")){
							level = 2;
						} else if (levelstr.equalsIgnoreCase("COLECTOR")){
							level = 3;
						}

						DEUser user = new DEUser(
								usernode.query("IDUSUARIO").get(0).getValue(),
								usernode.query("PASS").get(0).getValue(),
								level
								);
						user.setAction(usernode.query("Accion").get(0).getValue());
						userList.put(user);
					}
					
					statusObj.put("users", userList);
					queues.get("api").add(statusObj);
				} else {
					DEApp.log().info("MQWorker: La operacion {} no esta parametrizada", mqOperation);
					queues.get("mq").add(DEWorker.DECommand(DECommand.DATA));
				}
				
			} catch (Exception e) {
				
				
				DEApp.log().error("Error MQ", e);
				
				
				Element response = new Element("BovedaElectronicaResponse");
				
				Element node =  new Element("ErrorCode");
				node.appendChild("Err");
				response.appendChild(node);

				node =  new Element("ErrorText");
				node.appendChild(e.toString());
				response.appendChild(node);
				
				try {
					if (e instanceof MQException){
						TimeUnit.MINUTES.sleep(1);
						DEApp.log().error("Error DataWorker", e);
						JSONObject errorobj = new JSONObject();
						errorobj.put("DECommand", DECommand.ERRORDATABASE);
						errorobj.put("ID", "0");
						errorobj.put("UserID", "00000");
						errorobj.put("errortype", "ErrorMQ");
						errorobj.put("error", e.getMessage());
						errorobj.put("cause", e.getCause());
						StringBuilder stack = new StringBuilder();
						StackTraceElement[] stackTrace = e.getStackTrace(); 
						for (int i = 0; i < stackTrace.length;i++){
							stack.append(stackTrace[i]).append("\n");
						}
						errorobj.put("stack", stack.toString());
						queues.get("sync").add(errorobj);
					}

					mq.putMessage(response.toXML(), message);
				} catch (Exception e1) {
					DEApp.log().error("Error MQ Enviando Mensaje", e);
				}
				
				queues.get("mq").add(DEWorker.DECommand(DECommand.DATA));
			}
			break;
		case DATARESPONSE:
			try {
				DEFunctions func = (DEFunctions)obj.get("DEFunction");
				
				Document document;
				Element nodes;

				Element node;

				
				switch (func){
					case STATUS:
						document = (Document)obj.get("xml");
						nodes = document.getRootElement();

						node =  new Element("DESTATUS");
						node.appendChild(obj.getString("DEStatus"));
						nodes.appendChild(node);

						
						mq.putMessage(nodes.toXML().replaceAll("BovedaElectronicaRequest", "BovedaElectronicaResponse"), (MQMessage)obj.get("message"));
						break;
					case DATA:
						document = (Document)obj.get("xml");
						nodes = document.getRootElement();

						Text cdata = new Text(obj.getString("DEStatus"));
						node =  new Element("Data");
						node.appendChild(cdata);
						DEApp.log().debug("Contenido DB MQ: {}", cdata.toString());
						nodes.appendChild(node);
						DEApp.log().debug("Contenido XML DB MQ: {}", nodes.toXML().replaceAll("BovedaElectronicaRequest", "BovedaElectronicaResponse"));
						mq.putMessage(nodes.toXML().replaceAll("BovedaElectronicaRequest", "BovedaElectronicaResponse"), (MQMessage)obj.get("message"));
						break;
					case USERS:
						document = (Document)obj.get("xml");
						nodes = document.getRootElement();

						node =  new Element("ErrorCode");
						node.appendChild(obj.getString("DEStatus"));
						nodes.appendChild(node);
						
						node =  new Element("ErrorText");
						node.appendChild(obj.getString("code"));
						nodes.appendChild(node);
						
						mq.putMessage(nodes.toXML().replaceAll("BovedaElectronicaRequest", "BovedaElectronicaResponse"), (MQMessage)obj.get("message"));
						break;
					default:
						DEApp.log().debug("MQ Data response funcion {} no parametrizada", func.toString());
						break;
				}
			} catch (Exception  e) {
				DEApp.log().error("Error MQ", e);
			}
			queues.get("mq").add(DEWorker.DECommand(DECommand.DATA));
			break;
		default:
			DEApp.log().debug("MQWorker funcion {} no parametrizada", command.toString());
			break;
		}
	}
}
//<?xml version="1.0" encoding ="UTF-16"?><SiebelMessage MessageType="Integration Object" IntObjectName="BHD Cuentas Consolidadas" MessageId="" IdSol="" ErrorCode="" ErrorText=""><ListOfSiebelOmFinsEbanking><ListOfBHDCuentasConsolidadas><BHDCuentasConsolidadas><AccountStatus>ACTIVA</AccountStatus><AccountNumber>14985500016</AccountNumber><CommitmentAmount>0</CommitmentAmount><SaldoTotal>0</SaldoTotal><CurrentBalance>235.45</CurrentBalance><BalanceDisponible>235.45</BalanceDisponible><CCAvailableLimit>0</CCAvailableLimit><CapitalActual>0</CapitalActual><ErrorCode></ErrorCode><ErrorText></ErrorText><Moneda>000</Moneda></BHDCuentasConsolidadas><BHDCuentasConsolidadas><AccountStatus>VIGENTE-(SIN ATRASO)</AccountStatus><AccountNumber>3263786</AccountNumber><CommitmentAmount>53093.51</CommitmentAmount><SaldoTotal>0</SaldoTotal><CurrentBalance>0</CurrentBalance><BalanceDisponible>0</BalanceDisponible><CCAvailableLimit>0.00</CCAvailableLimit><CapitalActual>0</CapitalActual><ErrorCode></ErrorCode><ErrorText></ErrorText><Moneda>000</Moneda></BHDCuentasConsolidadas></ListOfBHDCuentasConsolidadas></ListOfSiebelOmFinsEbanking></SiebelMessage>
//<?xml version="1.0" encoding ="UTF-16"?><SiebelMessage IntObjectName="BHD Employee IO (OutPut)" MessageType="Integration Object" IntObjectFormat="Siebel Hierarchical" MessageId="" ErrorCode="" ErrorText=""><ListOfBhdEmployeeIo><Employee><CodigoEjecutivo>ALEXANP</CodigoEjecutivo><CodigoGestor>CECILIGR</CodigoGestor><CodigoSucursal>0101</CodigoSucursal><CodigoUnidadNeg>200</CodigoUnidadNeg><CodigoZona>T</CodigoZona><Ejecutivo>ALEXANDER PENA</Ejecutivo><GestorGeneral>GESTOR OP</GestorGeneral><Sucursal>OFICINA PRINCIPAL</Sucursal><UnidadNegocio>BANCA PERSONAL</UnidadNegocio><Zona>METRO CENTRO</Zona></Employee></ListOfBhdEmployeeIo></SiebelMessage>


