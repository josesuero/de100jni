package com.mstn.DE.sync;

import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQQueueManager;
import com.mstn.DE.DEApp;
import com.ibm.mq.MQC;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQGetMessageOptions;
import java.io.*;

public class DEMQ {
	
	private final int openPutQueueOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_OUTPUT; // OPCIONES DE CONEXION A LAS COLAS
	private final int openGetQueueOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_INPUT_AS_Q_DEF; // OPCIONES DE CONEXION A LAS COLAS
	private MQQueue putQueue;
	private MQQueue getQueue;
	private byte[] mqKey;
	
	private MQQueueManager queueManager;
	
	public MQQueueManager getQueueManager() {
		return queueManager;
	}

	public void setQueueManager(MQQueueManager queueManager) {
		this.queueManager = queueManager;
	}

	public DEMQ(){
		try{
			MQEnvironment.hostname = DEApp.getConfig("mqserver");
			MQEnvironment.channel = DEApp.getConfig("mqchannel");
			MQEnvironment.port = Integer.valueOf(DEApp.getConfig("mqport"));
		} catch (Exception e) {
			DEApp.log().error("Error MQ", e);
		}

	}
	
	public void connect(byte[] mqKey) throws MQException, IOException{
		this.mqKey = mqKey;
		DEApp.log().debug("MQ Key: {}", mqKey);
		queueManager = new MQQueueManager(DEApp.getConfig("mqmanager"));
		if (queueManager.isConnected()) {

			// ACCESANDO LA COLA DE ENTRADA
			DEApp.log().info(" Accesando la cola de entrada");
			putQueue = queueManager.accessQueue(DEApp.getConfig("mqreponse"), openPutQueueOptions);

			// ACCESANDO LA COLA DE SALIDA
			getQueue = queueManager.accessQueue(DEApp.getConfig("mqrequest"), openGetQueueOptions);

		} else {
			DEApp.log().error(" No Pudo Conectarse");
		}
	}
	
	public void putMessage(String responsexml, MQMessage message) throws IOException, MQException{
		// COLOCAR MENSAJE
		MQMessage putMessage = new MQMessage();
		MQPutMessageOptions pmo = new MQPutMessageOptions();
		

		putMessage.format = MQC.MQFMT_STRING;
		putMessage.messageId = message.messageId; //MQC.MQMI_NONE;// AQUI DEBES COLOCAR
												// LA LLAVE GENERADA
												// EN LA PC. VER EL
												// DOCUMENTO TECNICO
												// QUE FUE
												// COMPARTIDO
		putMessage.correlationId = message.correlationId;

		putMessage.clearMessage();
		putMessage.writeString(responsexml);
		//("<SiebelMessage><ListOfBHDEAIGeneralFuntionsParm><Function><FunctionName>MSG0100030B</FunctionName><IdSol>635079264947225646</IdSol><RequestDate>06/27/2013</RequestDate><RequestTime>10:41:34</RequestTime><UserName>SADMIN</UserName><Parameter01>0843543</Parameter01><Parameter02>USD</Parameter02></Function></ListOfBHDEAIGeneralFuntionsParm></SiebelMessage>");

		DEApp.log().info(" Colocando el mensaje en la cola de entrada");
		putQueue.put(putMessage, pmo);// ESTO NO ES NECESARIO AQUI
										// YA QUE LA APLICACION EN
										// ESTE MOMENTO ES UN
										// CLIENTE. ESTA PARTE DEBE
										// SER UTILIZADA LUEGO DE
										// REALIZADA LA OPERACIÓN
										// SOBRE LA BOVEDA PARA
										// ESCRIBIR LA RESPUESTA
		DEApp.log().info("El message id es " + bytArrayToHex(putMessage.messageId));

	}
	
	public MQMessage getMessage() throws IOException, MQException{
		if (queueManager == null || !queueManager.isConnected()){
			connect(mqKey);
		}
		MQGetMessageOptions gmo = new MQGetMessageOptions();
		// OBTENIENDO EL MENSAJE DE SALIDA
		gmo.options = MQC.MQGMO_WAIT;
		gmo.waitInterval = Integer.valueOf(-1); // AQUI EL VALOR
													// DEBE SER
													// INFINITO PARA
													// QUE SE QUEDE
													// LEYENDO LOS
													// MENSAJES
													// SIEMPRE
		gmo.matchOptions = MQC.MQMO_MATCH_MSG_ID;
		
		MQMessage getMessage = new MQMessage();
		//getMessage.clearMessage();
		//getMessage.correlationId = putMessage.messageId;
		getMessage.messageId = mqKey;

		DEApp.log().info(" Obteniendo el mensaje de la cola de salida");
		getQueue.get(getMessage, gmo);
		return getMessage;
	}
	
	public void close() throws MQException{
		// CERRANDO LA COLA DE ENTRADA
		DEApp.log().info(" Cerrando la cola de entrada");
		putQueue.close();

		// CERRANDO LA COLA DE SALIDA
		DEApp.log().info(" Cerrando la cola de salida");
		getQueue.close();
		
		if (queueManager != null) {
			// CERRANDO EL QUEUE MANAGER
			queueManager.close();

			// DESCONECTANDO EL QUEUE MANAGER
			DEApp.log().info(" Deconectando el queue manager");
			queueManager.disconnect();
		}

	}

	public static String bytArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder();
		for (byte b : a)
			sb.append(String.format("%02x", b & 0xff));
		return sb.toString();
	}
}
