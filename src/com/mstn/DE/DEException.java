package com.mstn.DE;

public class DEException extends Exception {
	private static final long serialVersionUID = 1L;

	public DEException(String error){
		super(error);
	}
}
