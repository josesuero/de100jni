package com.mstn.DE.data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

public class DESql {

	Connection c;
	
	enum DEDataType{
		INT, BIGINT, TEXT, DATE
	}

	public DESql() throws ClassNotFoundException, SQLException {
		createTables();
	}

	private Connection connect() throws SQLException, ClassNotFoundException {
		c = null;
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:de100jni.db");
		return c;
	}

	private boolean createTables() throws ClassNotFoundException, SQLException {

		String sql = new StringBuilder("CREATE TABLE IF NOT EXISTS LOG") 
			.append("(SEQUENCE INTEGER PRIMARY KEY AUTOINCREMENT not null")
			.append(" ,ID_TYPE           TEXT    NOT NULL")
			.append(" ,FECHAHORA            BIGINT     NOT NULL") 
			.append(" ,DATA        TEXT")
			.append(" ,SYNC         INT")
			.append(" ,ERRORCODE TEXT")
			.append(" ,COMPROBANTE TEXT")
			.append(" ,COLLECTION INT")
			.append(" ,CLOSING INT")
			.append(" ,EVENT INT")
			.append(")").toString();
		nonquery(sql);
		return true;
	}

	public JSONArray query(String sql) throws ClassNotFoundException, SQLException {
		JSONArray result = new JSONArray();
		connect();
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			ResultSetMetaData rsmd = rs.getMetaData();
			JSONObject jobject = new JSONObject();
			for (int i =1;i <= rsmd.getColumnCount();i++){
				jobject.put(rsmd.getColumnName(i), rs.getObject(i));
			}
			result.put(jobject);
		}
		rs.close();
		stmt.close();
		c.close();
		return result;
	}

	public int nonquery(String sql, ArrayList<SimpleEntry<DEDataType,Object>> args) throws ClassNotFoundException, SQLException {
			connect();
			PreparedStatement stmt = c.prepareStatement(sql);
			for (int i = 1 ;i <= args.size();i++){
				SimpleEntry<DEDataType,Object> arg = args.get(i - 1);
				switch(arg.getKey()){
				case INT:
					stmt.setInt(i, (Integer)arg.getValue());
					break;
				case BIGINT:
					stmt.setLong(i, (Long)arg.getValue());
					break;
				case TEXT:
					stmt.setString(i, (String)arg.getValue());
					break;
				case DATE:
					stmt.setDate(i, (Date)arg.getValue());
					break;
				}
			}
			
			stmt.executeUpdate();
			
			ResultSet rs = stmt.getGeneratedKeys();
			int genId = 0;
			if (rs.getMetaData().getColumnCount() > 0){
				genId = rs.getInt(1);
			}
			
			stmt.close();
			// c.commit();
			c.close();
			return genId;
	}
	
	public void nonquery(String sql) throws ClassNotFoundException, SQLException {
		connect();

		Statement stmt = c.createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
		// c.commit();
		c.close();
	}

}
