package com.mstn.DE.data;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@SuppressWarnings("unchecked")
public class DEXML {
	
	public static String Obj2XML(Object obj) throws JAXBException{

		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(obj.getClass());
		Marshaller m = context.createMarshaller();
		m.marshal(obj, writer);
		
		return writer.toString();
        
        //JAXBContext contextB = JAXBContext.newInstance(DEDataObject.class);
        //Unmarshaller unmarshallerB = contextB.createUnmarshaller();
        //JAXBElement<DEDataObject> jaxbElementB = unmarshallerB.unmarshal(sourceA, DEDataObject.class);

        //DEDataObject studentB = jaxbElementB.getValue();
        //DEApp.log().debug(studentB.getRequestId());
	}
	
	public static <T> T XML2Obj(String input,  Class<T>  objClass) throws JAXBException{
		JAXBContext context = JAXBContext.newInstance(objClass);
		Unmarshaller m = context.createUnmarshaller();
		return (T)m.unmarshal(new StringReader(input));
	}

}
