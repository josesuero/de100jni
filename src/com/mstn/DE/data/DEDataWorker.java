package com.mstn.DE.data;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mstn.DE.DEApp;
import com.mstn.DE.data.DESql.DEDataType;
import com.mstn.DE.workers.DEWorker;

public class DEDataWorker extends DEWorker {

	DESql deSql;
	
	String deleteCommand;

	public DEDataWorker(ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues)
			throws ClassNotFoundException, SQLException, IOException {
		super("data", queues);
		deSql = new DESql();
		
		if (DEApp.getConfig("delete") == null || DEApp.getConfig("delete").equals("1")){
			deleteCommand = "delete from log";
		} else{
			deleteCommand = "update log set sync = 3";
			
		}
	}

	private void storeEvent(JSONObject obj, DECommand command) throws ClassNotFoundException, SQLException {
		// int id = DEApp.genRequestID(obj);
		String sql = new StringBuilder("select SEQUENCE from LOG where event =").append(obj.getInt("EventNumber"))
				.toString();
		JSONArray logs = deSql.query(sql);
		if (logs.length() == 0) {
			sql = "insert into LOG(ID_TYPE, FECHAHORA, DATA, SYNC, COLLECTION, ERRORCODE, COMPROBANTE, EVENT, CLOSING) values (?,?,?,?,?,?,?,?,?)";
			ArrayList<SimpleEntry<DEDataType, Object>> args = new ArrayList<>();

			// args.add(new SimpleEntry<DESql.DEDataType,
			// Object>(DEDataType.INT,
			// id));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, command.toString()));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.BIGINT, new Date().getTime()));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, obj.toString()));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, 0));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, 0));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, ""));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, ""));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, obj.getInt("EventNumber")));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, 0));

			int id = deSql.nonquery(sql, args);
			obj.put("ID", id);

			sql = "update log set data = ? where sequence = ?";

			args = new ArrayList<>();
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, obj.toString()));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, id));
			deSql.nonquery(sql, args);

			// Enviar a syncronizacion
			queues.get("sync").add(obj);
		} else {
			obj.put("ID", logs.getJSONObject(0).get("SEQUENCE"));
			DEApp.log().info("Deposito ya existe");
		}
	}

	private void flagEvent(JSONObject obj, DECommand command, int SyncID, String ErrorCode, String Comprobante)
			throws ClassNotFoundException, SQLException {
		int id = DEApp.genRequestID(obj);
		String sql = new StringBuilder("select sequence from LOG where sequence = ").append(id).toString();

		JSONArray logs = deSql.query(sql);
		if (logs.length() > 0) {
			sql = "update LOG set SYNC = ?, ERRORCODE = ?, COMPROBANTE = ? where sequence = ?";
			ArrayList<SimpleEntry<DEDataType, Object>> args = new ArrayList<>();
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, SyncID));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, ErrorCode));
			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.TEXT, Comprobante));

			args.add(new SimpleEntry<DESql.DEDataType, Object>(DEDataType.INT, id));
			deSql.nonquery(sql, args);
			DEApp.log().info("Evento {} {} syncronizado", command.toString(), id);
		} else {
			DEApp.log().info("Deposito no existe o ya esta marcado");
		}
	}

	public void doWork(JSONObject objOrg) {
		try {
			JSONObject obj = new JSONObject(objOrg, JSONObject.getNames(objOrg));
			DEApp.log().debug("working data");
			DECommand command = (DECommand) obj.get("DECommand");
			switch (command) {
			case DEPOSIT:
				DEApp.log().info("Almacenando deposito");
				storeEvent(obj, command);
				queues.get("api").add(obj);
				break;
			case DEPOSITDONE:
				DEApp.log().info("Marcando deposito como exitoso");
				String errorCode = obj.getString("SYNCERRORCODE");
				String comprobante = obj.getString("SYNCCOMPROBANTE");

				flagEvent(obj, command, 1, errorCode, comprobante);
				break;
			case DEPOSITFAILED:
				DEApp.log().info("Marcando deposito fallido");
				flagEvent(obj, command, 2, "", "");
				break;
			case COLLECTION:
				DEApp.log().info("Almacenando coleccion");
				storeEvent(obj, command);
				// marcar depositos pendientes
				String sql = new StringBuilder("update log set collection = ").append(DEApp.genRequestID(obj))
						.append(" where collection = 0").toString();
				deSql.nonquery(sql);
				// marcar evento en maquina y solicitar el siguiente
				queues.get("api").add(obj);
				break;
			case COLLECTIONDONE:
				DEApp.log().info("Eliminando coleccion"); 
				String sqlCollection = new StringBuilder(deleteCommand).append(" where (collection = ")
						.append(DEApp.genRequestID(obj)).append(" or sequence = ").append(DEApp.genRequestID(obj))
						.append(") and ID_TYPE != '").append(DECommand.CLOSING.toString())
						.append("'")
						.toString();
				//DEApp.log().debug("Query de Eliminar {}", sqlCollection);
				deSql.nonquery(sqlCollection);
				break;
			case COLLECTIONFAILED:
				DEApp.log().info("Marcando coleccion fallida");
				flagEvent(obj, command, 2, "", "");
				// DEApp.log().debug(deSql.query("select * from log"));
				break;

			case CLOSING:
				DEApp.log().info("Almacenando cierre");
				storeEvent(obj, command);
				// marcar depositos pendientes
				String sqlClosing = new StringBuilder("update log set closing = ").append(DEApp.genRequestID(obj))
						.append(" where closing = 0").toString();
				deSql.nonquery(sqlClosing);
				// marcar evento en maquina y solicitar el siguiente
				queues.get("api").add(obj);
				break;
			case CLOSINGDONE:
				DEApp.log().info("Eliminando cierre ");
				String sqlClosingDone = new StringBuilder(deleteCommand).append(" where sequence = ")
						.append(DEApp.genRequestID(obj)).toString();
				deSql.nonquery(sqlClosingDone);
				break;
			case CLOSINGFAILED:
				DEApp.log().info("Marcando cierre fallido");
				flagEvent(obj, command, 2, "", "");
				// DEApp.log().debug(deSql.query("select * from log"));
				break;
			case END:
				DEApp.log().info("Finalizando data ");
				break;
			case DATA:
				JSONArray data = deSql.query("select * from log");
				for (int i = 0; i < data.length(); i++) {
					JSONObject dataobj = data.getJSONObject(i);
					dataobj.put("DATA", new JSONObject(dataobj.getString("DATA")));
				}

				String value = data.toString();
				obj.put("DEStatus", value);
				obj.put("DECommand", DECommand.DATARESPONSE);

				queues.get(obj.getString("worker")).add(obj);
				break;
			case IDCHANGE:
				DEApp.log().info("Almacenando cambio ID");
				try {
					storeEvent(obj, command);
				} catch (Exception e) {
					DEApp.log().error("error", e);
				}
				// marcar evento en maquina y solicitar el siguiente
				queues.get("sync").add(obj);
				break;
			case IDCHANGEDONE:
				DEApp.log().info("Eliminando change ");
				String sqlChangeDone = new StringBuilder(deleteCommand).append(" where sequence = ")
						.append(DEApp.genRequestID(obj)).toString();
				deSql.nonquery(sqlChangeDone);
				break;
			case IDCHANGEFAILED:
				DEApp.log().info("Marcando idchange fallido");
				flagEvent(obj, command, 2, "", "");
				// DEApp.log().debug(deSql.query("select * from log"));
				break;

			default:
				DEApp.log().debug("DataWorker funcion {} no parametrizada", command.toString());
				break;
			}
			// DEApp.log().debug(deSql.query("select * from log"));
		} catch (ClassNotFoundException | SQLException e) {
			DEApp.log().error("Error DataWorker", e);
			JSONObject errorobj = new JSONObject();
			errorobj.put("DECommand", DECommand.ERRORDATABASE);
			errorobj.put("ID", "0");
			errorobj.put("UserID", "00000");
			errorobj.put("errortype", "ErrorDatabase");
			errorobj.put("error", e.getMessage());
			errorobj.put("cause", e.getCause());
			StringBuilder stack = new StringBuilder();
			StackTraceElement[] stackTrace = e.getStackTrace(); 
			for (int i = 0; i < stackTrace.length;i++){
				stack.append(stackTrace[i]).append("\n");
			}
			errorobj.put("stack", stack.toString());

			queues.get("sync").add(errorobj);
		}
	}
}
