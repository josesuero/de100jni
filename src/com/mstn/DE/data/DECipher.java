package com.mstn.DE.data;

import java.io.*;
import java.security.*;
import java.security.KeyStore.PasswordProtection;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class DECipher {
    private static final String ALGORITHM = "AES";
    //private static final byte[] keyValue = "ADBSJHJS12547896".getBytes();
    private static final String keyStorePass = "DF4343FAFWFD3FQE";
    private static final String keyEntryPass = "SDFASW43Q3EFSD33"; 
    private static final String keyStoreFile = "keystore";

    public static String encrypt(String valueToEnc) throws Exception {

        Key key = retrieveKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);

        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        String encryptedValue = new Base64().encodeToString(encValue);

        return encryptedValue;
    }

    public static String decrypt(String encryptedValue) throws Exception {
        SecretKey key = retrieveKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodedValue = new Base64().decode(encryptedValue.getBytes());
        byte[] decryptedVal = c.doFinal(decodedValue);
        return new String(decryptedVal);
        
    }

    private static KeyStore createKeyStore(String fileName, String pw) throws Exception {
        File file = new File(fileName);
     
        final KeyStore keyStore = KeyStore.getInstance("JCEKS");
        if (file.exists()) {
            // .keystore file already exists => load it
            keyStore.load(new FileInputStream(file), pw.toCharArray());
        } else {
            // .keystore file not created yet => create it
            keyStore.load(null, null);
            keyStore.store(new FileOutputStream(fileName), pw.toCharArray());
        }
     
        return keyStore;
    }

    public static boolean storeKey(String key) throws Exception{
        KeyStore keyStore = createKeyStore(keyStoreFile, keyStorePass);
        
        // generate a secret key for AES encryption
        //SecretKey secretKey = KeyGenerator.getInstance("AES").generateKey();
        SecretKey secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
     
        // store the secret key
        KeyStore.SecretKeyEntry keyStoreEntry = new KeyStore.SecretKeyEntry(secretKey);
        PasswordProtection keyPassword = new PasswordProtection(keyEntryPass.toCharArray());
        keyStore.setEntry("mySecretKey", keyStoreEntry, keyPassword);
        keyStore.store(new FileOutputStream(keyStoreFile), keyStorePass.toCharArray());
     
        return true;
    }
    
    private static SecretKey retrieveKey() throws Exception{
    	KeyStore keyStore = createKeyStore(keyStoreFile, keyStorePass);
    	PasswordProtection keyPassword = new PasswordProtection(keyEntryPass.toCharArray());

        // retrieve the stored key back
        KeyStore.Entry entry = keyStore.getEntry("mySecretKey", keyPassword);
        SecretKey keyFound = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
        return keyFound;

    }
    
}
