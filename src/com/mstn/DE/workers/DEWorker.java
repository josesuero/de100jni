package com.mstn.DE.workers;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;

import com.mstn.DE.DEApp;

public abstract class DEWorker extends Thread implements Callable<DEWorker> {

	protected ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues;
	protected String workerName;

	public enum DECommand {
		START, END, EVENT, SYNC, 
		DATA, DATARESPONSE, 
		DEPOSIT, DEPOSITDONE, DEPOSITFAILED, 
		CLOSING, CLOSINGDONE, CLOSINGFAILED, 
		COLLECTION, COLLECTIONDONE, COLLECTIONFAILED,
		STATUS, USERS,
		IDCHANGE, IDCHANGEDONE, IDCHANGEFAILED,
		ERROR, ERRORDATABASE
	}
	
	public enum DEStatus{
		OK, FAILED
	}
	
	public enum DEFunctions {
		STATUS, FIXEVENT, COUNTERSTATUS, UPDATE, DOWNLOAD, USERS, DATA
	}
	
	private DEStatus status = DEStatus.OK;

	protected boolean end = false;

	public DEWorker(String workerName, ConcurrentHashMap<String, BlockingQueue<JSONObject>> queues) {
		this.workerName = workerName;
		this.queues = queues;
	}
	

	public void run() {
		try {
			while (!end) {
				JSONObject obj = queues.get(workerName).take();
				doWork(obj);
				if (obj.has("DECommand") && obj.get("DECommand") == DECommand.END) {
					end = true;
				}

			}
		} catch (Exception ie) {
			DEApp.log().error("Error worker", ie);
		}
	}

	@Override
	public DEWorker call() {
		this.run();
		return this;
	}

	public static JSONObject DECommand(DECommand command) {
		return new JSONObject().put("DECommand", command);
	}

	public abstract void doWork(JSONObject s);


	public DEStatus getStatus() {
		return status;
	}

	public void setStatus(DEStatus status) {
		this.status = status;
	}

}
