package com.mstn.DE.file;

import com.mstn.DE.DEApp;
 

public class MainClass {
	
	public static void main(String[] args) {
		try {
			DEFile reader = new DEFile();
			reader.readSetFile(null);
			reader.readUsers();

			// reader.userDelete("1444");
			// reader.userDelete("1555");
			// reader.userDelete("1666");
			//
			// reader.userAdd(new DEUser("1231","1234",1));
			// reader.userAdd(new DEUser("1232","1234",2));
			// reader.userAdd(new DEUser("1233","1234",3));
			// reader.userAdd(new DEUser("1234","1234",1));
			// reader.userAdd(new DEUser("1235","1234",2));
			// reader.userAdd(new DEUser("1236","1234",3));
			// reader.userAdd(new DEUser("1237","1234",1));
			// reader.userAdd(new DEUser("1238","1234",2));
			// reader.userAdd(new DEUser("1239","1234",3));
			// reader.userAdd(new DEUser("1240","1234",1));
			// reader.userAdd(new DEUser("1241","1234",2));
			// reader.userAdd(new DEUser("1242","1234",3));
			//
			//
			// //reader.userUpdate(new DEUser("1444","1777",1));
			// //reader.userDelete("1555");
			//
			for (DEUser user : reader.getUsers()){
				DEApp.log().info("{} - {} - {} ",user.getUserID(), user.getPassword(), user.getLevel());
			}
			//
			reader.fillUsers();
			reader.createFile();
			reader.createTextFile();

			
		} catch (Exception e) {
			DEApp.log().error("Error mainclass", e);
		}

	}

}
