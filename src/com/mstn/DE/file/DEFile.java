package com.mstn.DE.file;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.mstn.DE.DEApp;

public class DEFile {

	private final int DE_MULTI_ = 8; // MULTI Data Size
	private final int DE_SIZE_ = 6142; // Data Size
	private final int DE_ALL_SIZE_ = 20000; // File Size
	// private final int DE_ALL_SIZE_2 = 9276; // File Size DLL

	private final int START_ = 2; // Start Row
	private final int END_ = 8297; // End Row

	private final int NAME_ = 2; // Name Col
	// private final int DEFAULT_ = 8; // Default Col
	private final int INPUT_ = 9; // Data Input Col
	private final int INPUT_IdPass = INPUT_ + 19; // Id Password Input Col

	private final int ID_ = 1433;
	private final int PASSWORD_ = 7813;

	// Manual Setting Default
	// private final String Manual_01 = "UNFIT NOTE";
	// private final String Manual_02 = "COIN";
	// private final String Manual_03 = "CHECK1";
	// private final String Manual_04 = "CHECK2";
	// private final String Manual_05 = "CHECK3";
	// private final String Manual_06 = "CHECK4";
	// private final String Manual_07 = "NOTE";

	// ID Default
	// private final String CATEGORY_NAME_IDRD_ = "ID Registration";
	//private final String EASY_ID_ = "00005433"; // Easy Mode ID
	//private final String DEFAULT_ID_ = "000000000"; // Default ID
	//
	// private final String IDENTIFIER = "INGL";
	private int filetype_;
	//private final int DE_ID_PASSWORD_START = 7820; // ID Password
	
	//private boolean ReEntFlgSh1; // Worksheet_Change()
	// private int UnProtectFlg; // Worksheet_Protect
	//private boolean ReSum; // SUM

	private ArrayList<ArrayList<String>> array;

	public DEFile() throws IOException {
		array = read();
		readUsers();
	}

	private String addCells(int row, int column, String value) {
		return array.get(row).set(column, value);
	}

	private String Hex(int value) {
		return String.format("%02X", value);
	}

	private String Bin2Hex(String value, int radix) {
		return Hex(Integer.parseInt(value.trim(), radix));
	}

	private String ExDeciToBin(String sdeci) {
		long deci = Long.valueOf(sdeci);
		long ln;
		String stemp;
		int i;
		int count;

		stemp = "1";
		count = Ex2noBeki(deci);
		ln = deci - (long) Math.pow(2, count);

		for (i = count - 1; i >= 0; i--) {
			if (ln < Math.pow(2, i)) {
				stemp = stemp + "0";
			} else {
				stemp = stemp + "1";
				ln = ln - (int) Math.pow(2, i);
			}
		}
		return stemp;
	}

	private int Ex2noBeki(long deci) {
		int i = 0;
		while (true) {
			if (deci < Math.pow(2, i)) {
				return i - 1;
			}
			i = i + 1;
		}
	}

	private byte CByte(String value) {
		return javax.xml.bind.DatatypeConverter.parseHexBinary(value)[0];
	}

	//private int lID_cnt;

//	private boolean Check_ID() {
//
//		int ID_Zero;
//		String wkID;
//
//		String wkPassword;
//		int IdPass_Zero;
//		int k;
//
//		lID_cnt = 0;
//		k = 0;
//
//		ID_Zero = 1433;
//		IdPass_Zero = DE_ID_PASSWORD_START;
//		int i;
//		for (i = ID_Zero; i <= ID_Zero + (399 * 15); i += 15) {
//			wkID = array.get(i).get(INPUT_);
//			if (Integer.valueOf(wkID) == 0) {
//				wkID = DEFAULT_ID_;
//			}
//			if (wkID != DEFAULT_ID_) {
//				lID_cnt = lID_cnt + 1;
//				for (int j = i + 15; j < ID_Zero + (400 * 15); j += 15) {
//					if (Integer.valueOf(wkID) == Integer.valueOf(array.get(j).get(INPUT_))) {
//						DEApp.log().info(array.get(i).get(INPUT_ - 7) + "[" + wkID + "]" + "\n"
//								+ array.get(j).get(INPUT_ - 7) + "[" + array.get(j).get(INPUT_) + "]");
//						return false;
//					}
//				}
//				// Password
//				wkPassword = array.get(i).get(INPUT_IdPass);
//				if (wkPassword != "") {
//					addCells(IdPass_Zero + k, INPUT_, String.valueOf(wkPassword));
//				} else {
//					addCells(IdPass_Zero + k, INPUT_, "0");
//				}
//			} else {
//				// ID
//				wkPassword = array.get(i).get(INPUT_IdPass);
//				if (wkPassword != "") {
//					if (Integer.valueOf(wkPassword) != 0) {
//						ReEntFlgSh1 = true;
//						addCells(i, INPUT_IdPass, "");
//						addCells(IdPass_Zero + k, INPUT_, "0");
//						ReEntFlgSh1 = false;
//					}
//				}
//			}
//			k = k + 1;
//		}
//
//		if (array.get(i).get(INPUT_) == EASY_ID_) {
//			lID_cnt = lID_cnt + 1;
//			if (!array.get(i + 1).get(INPUT_).equals("1")) {
//				ReEntFlgSh1 = true;
//				addCells(i + 1, INPUT_, "1");
//				addCells(i + 3, INPUT_, "0");
//				addCells(i + 7, INPUT_, "3");
//				ReEntFlgSh1 = false;
//			}
//		}
//
//		ReEntFlgSh1 = true;
//		addCells(ID_Zero - 1, INPUT_, String.valueOf(lID_cnt));
//		ReEntFlgSh1 = false;
//
//		return true;
//
//	}
//
//	private boolean Check_RegiID() {
//		int ID_Zero;
//		String wkID;
//
//		ID_Zero = 7616 - 7;
//		for (int i = ID_Zero; i <= ID_Zero + (97 * 2); i += 2) {
//			wkID = array.get(i).get(INPUT_);
//			if (!wkID.equals("0")) {
//				if (!array.get(i - 1).get(INPUT_).equals(array.get(i - 1).get(INPUT_ - 2))) {
//					addCells(i - 1, INPUT_, array.get(i - 1).get(INPUT_ - 2));
//				}
//
//				lID_cnt = lID_cnt + 1;
//				for (int j = i + 2; j < ID_Zero + (98 * 2); j += 2) {
//					if (Integer.valueOf(wkID) == Integer.valueOf(array.get(j).get(INPUT_))) {
//						DEApp.log().info(array.get(i).get(INPUT_ - 7) + "[" + wkID + "]" + "\n"
//								+ array.get(j).get(INPUT_ - 7) + "[" + array.get(j).get(INPUT_) + "]");
//						return false;
//					}
//				}
//			}
//		}
//
//		return true;
//
//	}

	private ArrayList<ArrayList<String>> read() throws IOException {
		RandomAccessFile aFile = new RandomAccessFile("../files/settings.csv", "r");
		FileChannel fc = aFile.getChannel();

		ByteBuffer buf = ByteBuffer.allocate(4800);
		int bytesRead;

		boolean quote = false;

		String value = "";
		int row = 0;

		ArrayList<ArrayList<String>> array = new ArrayList<>();
		array.add(new ArrayList<String>());
		//array.get(row).add("");
		do {
			buf.clear();
			bytesRead = fc.read(buf);
			// DEApp.log().info("Read" + bytesRead);
			buf.flip();

			while (buf.hasRemaining()) {
				char currChar = (char) buf.get();
				int ascii = (int) currChar;
				if (currChar == ',' && !quote) {
					array.get(row).add(value);
					value = "";
				} else if (ascii == 13 ) {
				} else if ((ascii == 10 ) && quote) {
					// value += " ";
					value += currChar;
				} else if (ascii == 10 && !quote) {
					array.get(row).add(value);
					array.add(new ArrayList<String>());
					value = "";
					row += 1;
				} else if (currChar == '"' && !quote) {
					quote = true;
				} else if (currChar == '"' && quote) {
					quote = false;
				} else {
					value += currChar;
				}
				// DEApp.log().info((int) currChar);
			}
		} while (bytesRead != -1);
		aFile.close();
		return array;
	}

	private int bufferSize = 48;
	private ByteBuffer fileBuffer;
	private FileChannel fc;

	private void writeFile() throws IOException {
		fileBuffer.flip();

		while (fileBuffer.hasRemaining()) {
			fc.write(fileBuffer);
		}
		fileBuffer.clear();

	}

	//private int byteNum = -1;

	private void Put(byte buf) throws IOException {
		fileBuffer.put((byte) (buf & 0xFF));
		if (fileBuffer.capacity() <= fileBuffer.position()) {
			writeFile();
		}
		//byteNum++;
	}

	public void createFile() throws IOException {
		String path = "../files/newdata.set";
		// ArrayList<ArrayList<String>> array = read();
		try {
			Files.delete(Paths.get(path));
		} catch (IOException e) {
			// file does not exists;
		}
		RandomAccessFile aFile = new RandomAccessFile(path, "rw");

		fileBuffer = ByteBuffer.allocate(bufferSize);
		fc = aFile.getChannel();

		// String SFileName;
		// int SetNum;
		// int WriteNo;
		byte buf = 0;
		int lSize;
		String sType;
		int lSum;
		int dSum;

		dSum = 0;
		lSum = 0;

		/*
		if (!Check_ID()) {
			return;
		}
		if (!Check_RegiID())
			return;
		*/
		for (int i = 0; i < 8; i++) {
			buf = (byte) 0;
			dSum = dSum + (buf & 0xFF);
			// write to file
			Put(buf);
		}

		String sBuff = null;
		String cBuff = null;
		String sCategory;
		String sBuff2 = null;
		sCategory = array.get(START_).get(1);

		for (int i = START_; i < END_; i++) {
			if (array.get(i).get(0).equalsIgnoreCase("End"))
				break;

			if (!array.get(i).get(0).equals("")) {
				if (!array.get(i).get(INPUT_).equals("")) {
					if (array.get(i).get(0).equals("1516")) {
						"".toString();
					}
					lSize = Integer.valueOf(array.get(i).get(INPUT_ - 5));
					sType = array.get(i).get(INPUT_ - 4);

					if (array.get(i).get(INPUT_ - 6).equals("type")) {
						if (array.get(i).get(INPUT_).equals("0")) {
							addCells(i, INPUT_, "1");
						}
					}

					if (array.get(i).get(INPUT_ - 7).equals("Idlength")) {
						if (array.get(i).get(INPUT_).equals("0")) {
							addCells(i, INPUT_, "4");
						}
					}

					if (lSize == 1) {
						int temp = Integer.valueOf(array.get(i).get(INPUT_));
						buf = (byte) temp;
						Put(buf);

						dSum = dSum + temp;
						if (sCategory.equals(array.get(i).get(1))) {
							lSum = lSum + temp;
						}
					} else {
						sBuff = "";
						if (sType.equals("HEX")) {
							sBuff = sBuff + array.get(i).get(INPUT_);

							if (sCategory.equals(array.get(i).get(1)) && array.get(i).get(2).equals("SUM")) {
								sBuff2 = Hex(lSum);
								int size = ((lSize * 2) - sBuff2.length());
								for (int j = 0; j < size; j++) {
									sBuff2 = "0" + sBuff2;
								}
								if (!sBuff.equals(sBuff2)) {
									sBuff = sBuff2;
									addCells(i, INPUT_, sBuff);
								}
								sCategory = array.get(i + 1).get(1);
								lSum = 0;
							}

							if (sBuff.length() < lSize * 2) {
								int size = ((lSize * 2) - sBuff.length());
								for (int j = 0; j < size; j++) {
									sBuff = "0" + sBuff;
								}
							}

							for (int j = 0; j < (lSize * 2); j += 2) {
								cBuff = sBuff.substring(j, j + 2);
								buf = CByte(cBuff);
								Put(buf);
								dSum = dSum + (buf & 0xFF);
								if (sCategory.equals(array.get(i).get(1)) && !array.get(i).get(2).equals("SUM")) {
									lSum = lSum + (buf & 0xFF);
								}
							}
						} else if (sType.equals("ASC")) {
							sBuff = array.get(i).get(INPUT_).trim();
							for (byte b : sBuff.getBytes()) {
								Put(b);
							}

							for (int j = 0; j < sBuff.length(); j++) {
								cBuff = sBuff.substring(j, j + 1);
								dSum = dSum + (int) cBuff.charAt(0);
								lSum = lSum + cBuff.charAt(0);
							}

							if (sBuff.length() < lSize) {
								buf = (byte) 32;
								int size = (lSize - sBuff.length());
								for (int j = 0; j < size; j++) {
									Put(buf);
									;
									dSum = dSum + (buf & 0xFF);
									if (sCategory.equals(array.get(i).get(1))) {
										lSum = lSum + (buf & 0xFF);
									}
								}
							}

						} else if (sType.equals("LLNG") || sType.equals("DEC")) {
							if (Long.valueOf(array.get(i).get(INPUT_).trim()) > 0) {
								sBuff = ExDeciToBin(array.get(i).get(INPUT_));
							} else {
								sBuff = array.get(i).get(INPUT_);
							}

							if (sBuff.length() < lSize * 8) {
								int size = (lSize * 8) - sBuff.length();
								for (int j = 0; j < size; j++) {
									sBuff = "0" + sBuff;
								}
							}

							for (int j = 0; j < lSize * 8; j += 8) {
								cBuff = sBuff.substring(j, j + 8);
								cBuff = Bin2Hex(cBuff, 2);
								buf = CByte(cBuff);
								Put(buf);
								dSum = dSum + (buf & 0xFF);
								lSum = lSum + (buf & 0xFF);
							}
						} else {
							DEApp.log().error("Error");
						}
					}
				}
			}

		}

		buf = (byte) 0;

		for (int i = 0; i < (DE_ALL_SIZE_ - (DE_MULTI_ + DE_SIZE_ + 4)); i++) {
			dSum = dSum + (buf & 0xFF);
			Put(buf);
		}

		sBuff = Hex(dSum);
		int size = (4 * 2 - sBuff.length());
		for (int j = 0; j < size; j++) {
			sBuff = "0" + sBuff;
		}
		for (int j = 0; j < 4 * 2; j += 2) {
			cBuff = sBuff.substring(j, j + 2);
			buf = CByte(cBuff);
			Put(buf);
		}

		buf = (byte) 0;
		for (int i = 0; i < 512 - (DE_ALL_SIZE_ % 512); i++) {
			Put(buf);
		}
		writeFile();
		fc.close();
		//ReSum = false;
		aFile.close();
		DEApp.log().info("Creacion de archivo compleado");
	}

	private ArrayList<DEUser> users = new ArrayList<>();

	private DEUser emptyUser() {
		DEUser user = new DEUser();
		user.setLevel(0);
		user.setUserID("000000000");
		user.setPassword("");
		return user;
	}

	private DEUser readUser(int pos) {
		DEUser user = new DEUser();
		int row = ID_ + (pos * 15);
		ArrayList<String> userRow = array.get(row);
		user.setUserID(userRow.get(INPUT_));
		user.setPassword(userRow.get(28));
		user.setLevel(Integer.valueOf(array.get(row + 1).get(INPUT_)));
		return user;
	}

	private void updateUserArray(int pos, DEUser user) {
		int row = ID_ + (pos * 15);
		ArrayList<String> userRow = array.get(row);
		userRow.set(INPUT_, user.getUserID());
		userRow.set(28, user.getPassword());
		array.get(row + 1).set(INPUT_, String.valueOf(user.getLevel()));
		array.get(row + 1).set(INPUT_ + 1, user.getLevelText());

		if (user.getLevel() == 0) {
			array.get(row + 3).set(INPUT_, "0");
			array.get(row + 7).set(INPUT_, "0");
		} else if (user.getLevel() == 1) {
			array.get(row + 3).set(INPUT_, "6");
			array.get(row + 7).set(INPUT_, "15");
		} else if (user.getLevel() == 2) {
			array.get(row + 3).set(INPUT_, "6");
			array.get(row + 7).set(INPUT_, "191");
		} else if (user.getLevel() == 3) {
			array.get(row + 3).set(INPUT_, "5");
			array.get(row + 7).set(INPUT_, "64");
		}

		String password = user.getPassword();
		if (password.equals(""))
			password = "0";
		array.get(PASSWORD_ + pos).set(INPUT_, password);
		DEApp.log().debug("Usuario {} agregado", user.getUserID());

	}

	public void fillUsers() {
		array.get(ID_ - 1).set(INPUT_, String.valueOf(users.size()));
		for (int i = 0; i < users.size(); i++) {
			updateUserArray(i, users.get(i));
		}
		for (int i = users.size(); i < 400; i++) {
			updateUserArray(i, emptyUser());
		}
	}

	public ArrayList<DEUser> readUsers() {
		users.clear();
		for (int i = 0; i < 400; i++) {
			DEUser user = readUser(i);
			if (Integer.valueOf(user.getUserID()) != 0) {
				users.add(user);
			}
		}
		return users;
	}

	public void userAdd(DEUser user) {
		int userPos = users.indexOf(user);
		if (userPos != -1) {
			users.set(userPos, user);
		} else {
			users.add(user);
		}
	}

	public void userUpdate(DEUser user) {
		int userPos = users.indexOf(user);
		if (userPos != -1) {
			users.set(userPos, user);
		}

	}

	public void userDelete(String id) {
		int userPos = users.indexOf(new DEUser(id, "", 0));
		if (userPos != -1) {
			users.remove(userPos);
		}
	}

	public ArrayList<DEUser> getUsers() {
		return users;
	}

	// File Reader

	//private boolean ID_invFlg = true;

	private long UIntToDbl(long uIntValue) {
		if (uIntValue < 0) {
			long value1 = (uIntValue & 2147483647);
			long result = new Double(Math.pow(2, 31)).longValue() + value1;
			return result;
		} else {
			return uIntValue;
		}
	}

	private long sl_undef(long x, int n) throws IOException, InterruptedException {

		long result;
		if (n == 0) {
			result = x;
		} else {
			long k;

			k = new Double(Math.pow(2, (32 - n - 1))).longValue();

			long d = x & (k - 1); // vbANDOR("and", x, (k-1));
			// d = x && (k - 1);

			long c = d * new Double(Math.pow(2, n)).longValue();

			if ((x & k) != 0) {
				c = (c ^ -2147483648);
				c = UIntToDbl(c);
			}
			result = c;
		}
		return result;
	}

	public void readSetFile(String filePath) throws IOException, InterruptedException {

		//String SFileName;
		//long SetNum;

		//int RdSiz;
 
		if (filePath == null) {
			filePath = "Z:\\mstn\\DE-100\\DE100Jni\\files\\20160712_DEVICE_SETTING.SET";
		}

		Path path = Paths.get(filePath);
		byte ar[] = Files.readAllBytes(path);

		//RdSiz = DE_ALL_SIZE_;
		filetype_ = 0;

		int i, j, k;
		// int l,
		int m;
		int num;
		String sBuff;
		//long lBuff;
		long lSize;
		String sType;
		long dlBuff;
		long dlBuff2;
		int llcnt;
		int iwkMulti;
		int iwkIdpos;

		if (filetype_ == 0) {
			iwkMulti = DE_MULTI_;
		} else {
			iwkMulti = 0;
		}
		iwkMulti = 0;
		num = START_;
		iwkIdpos = 0;

		for (i = iwkMulti; i < (iwkMulti + DE_SIZE_); i++) {
			for (j = num; j < END_; j++) {
				if (j + 7 == 7821)
					"".toString();
				if (array.get(j).get(0).equals("145"))
					"".toString();
				if (array.get(j).get(0).equals(String.valueOf(i - iwkMulti + 1))) {
					if (array.get(j).get(NAME_).length() > 2 && array.get(j).get(NAME_).substring(0, 3).equals("ID-")) {
						if (iwkIdpos == 0) {
							iwkIdpos = j;
						}
						if (ar[i + 5] == 0) {
							sType = "HEX";
						} else {
							sType = array.get(j).get(INPUT_ - 4);
						}
					} else {
						sType = array.get(j).get(INPUT_ - 4);
					}
					lSize = Long.valueOf(array.get(j).get(INPUT_ - 5));
					//
					if (lSize == 1) {
						if (!array.get(j).get(INPUT_).equals(String.valueOf(ar[i] & 0xFF))) {
							array.get(j).set(INPUT_, String.valueOf(ar[i]));
						}
						break;
					} else {
						sBuff = "";
						//lBuff = 0;
						dlBuff = 0;
						dlBuff2 = 0;
						llcnt = 0;

						for (k = i; k <= i + (lSize - 1); k++) {
							if (sType.equals("HEX")) {
								String temp = "0" + Hex(ar[k]);
								sBuff = sBuff + temp.substring(temp.length() - 2);
								// array.get(j).get(INPUT_).NumberFormat = "@";
								// array.get(j).get(INPUT_).HorizontalAlignment
								// = xlHAlignRight;
							} else if (sType.equals("ASC")) {
								sBuff = sBuff + (char)ar[k];
							} else if (sType.equals("DEC")) {
								dlBuff = sl_undef(dlBuff, 8);
								dlBuff = dlBuff + (ar[k] & 0xFF);
							} else if (sType.equals("LLNG")) {
								dlBuff2 = sl_undef(dlBuff2, 8);
								dlBuff2 = dlBuff2 + ar[k];
								if (llcnt == 3) {
									dlBuff = dlBuff2;
									dlBuff2 = 0;
									dlBuff = sl_undef(dlBuff, 8 * 4);
								}
								llcnt = llcnt + 1;
							} else {
								sBuff = sBuff + ar[k];
							}
						}
						// k++;
						if (sType.equals("DEC")) {
							if (!array.get(j).get(INPUT_).equals(dlBuff)) {
								array.get(j).set(INPUT_, String.valueOf(dlBuff));
							}
							if (array.get(j).get(NAME_).length() > 8
									&& array.get(j).get(NAME_).substring(0, 9).equals("Password-")) {
								// l = array.get(j).get(NAME_).length() -
								// array.get(j).get(NAME_).indexOf("-");
								m = Integer.valueOf(
										array.get(j).get(NAME_).substring(array.get(j).get(NAME_).indexOf("-") + 1));
								if (dlBuff != 0) {
									array.get(iwkIdpos + ((m - 1) * 15)).set(INPUT_IdPass, String.valueOf(dlBuff));
								} else {
									array.get(iwkIdpos + ((m - 1) * 15)).set(INPUT_IdPass, "");
								}
							}
						} else if (sType.equals("LLNG")) {
							dlBuff = dlBuff + dlBuff2;
							DecimalFormat myFormatter = new DecimalFormat("#0");
							sBuff = myFormatter.format(dlBuff);
							if (!array.get(j).get(INPUT_).equals(sBuff)) {
								array.get(j).set(INPUT_, sBuff);
							}
						} else {
							if (!array.get(j).get(INPUT_).equals(sBuff)) {
								array.get(j).set(INPUT_, sBuff);
							}
						}
						i = k - 1;
						break;
					}
				}
			}
			// break;
			num = j + 1;
		}
		DEApp.log().info("Lectura de archivo completado");
	}

	public void createTextFile() {
		Path path = Paths.get("../files/altsettings.csv");
		try {
			Files.delete(path);
		} catch (IOException e) {
			// file does not exists;
		}
		StringBuilder sb = new StringBuilder();

		for (ArrayList<String> line : array) {
			StringBuilder newline = new StringBuilder();
			for (int i = 0; i < line.size(); i++) {
				String col = line.get(i);
				if (i > 0)
					newline.append(",");
				if (col.contains("\n") || col.contains("\r")) {
					newline.append("\"").append(col).append("\"");
				} else {
					newline.append(col);
				}
			}
			sb.append(newline).append("\r\n");
		}

		Charset charset = Charset.forName("US-ASCII");
		String s = sb.toString();
		try (BufferedWriter writer = Files.newBufferedWriter(path, charset)) {
			writer.write(s, 0, s.length());
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
	}
}
