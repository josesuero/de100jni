package com.mstn.DE.file;

public class DEUser implements Comparable<DEUser> {

	private String userID;
	private int level;
	private String password;
	private String action;
	
	public DEUser(){
		
	}
	public DEUser(String userID, String password, int level){
		this.userID = userID;
		this.password = password;
		this.level = level;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLevelText() {
		switch (level) {
		case 0:
			return "0:-----";
		case 1:
			return "1:CHECKER";
		case 2:
			return "2:MANAGER";
		case 3:
			return "3:CIT";
		case 4:
			return "4:MAINTENANCE";
		case 5:
			return "5:ALMIGHTY";
		default:
			return "0:-----";
		}
	}
	@Override
	public int compareTo(DEUser o) {
		return Integer.valueOf(userID) - Integer.valueOf(o.userID);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DEUser){
			return ((DEUser)obj).getUserID().equals(userID);
		} else {
			return false;
		}
		//return super.equals(obj);
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
}
