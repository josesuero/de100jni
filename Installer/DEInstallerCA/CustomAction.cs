using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Diagnostics;

namespace DEInstallerCA
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult ConfigInfo(Session session)
        {
            String path = session.CustomActionData["InstallLocation"];

            frmConfig config = new frmConfig(path);
            if (config.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return ActionResult.UserExit;

            return ActionResult.Success;
        }
        [CustomAction]
        public static ActionResult Rollback(Session session)
        {
            String path = session.CustomActionData["InstallLocation"];
            Process cmd = new Process();
            cmd.StartInfo.FileName = @"c:\Windows\System32\net.exe";
            cmd.StartInfo.Arguments = "stop de100";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //MessageBox.Show(cmd.StandardOutput.ReadToEnd());

            cmd = new Process();
            cmd.StartInfo.FileName = path + "bin/wrapper.exe";
            cmd.StartInfo.Arguments = "-r ..\\conf\\wrapper.conf";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult Uninstall(Session session)
        {
            String path = session.CustomActionData["InstallLocation"];
            Process cmd = new Process();
            cmd.StartInfo.FileName = path + "bin/wrapper.exe";
            cmd.StartInfo.Arguments = "-r ..\\conf\\wrapper.conf";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //MessageBox.Show(cmd.StandardOutput.ReadToEnd());

            cmd = new Process();
            cmd.StartInfo.FileName = @"c:\Windows\System32\net.exe";
            cmd.StartInfo.Arguments = "start de100";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();

            return ActionResult.Success;
        }
    }
}
