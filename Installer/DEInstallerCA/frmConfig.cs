﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Text.RegularExpressions;
using Microsoft.Deployment.WindowsInstaller;
using System.Xml;
using System.Diagnostics;

namespace DEInstallerCA
{
    public partial class frmConfig : Form
    {

        String path;

        Dictionary<String, String> propiedadesDesarrollo = new Dictionary<String, String>();
        Dictionary<String, String> propiedadesSQA = new Dictionary<String, String>();
        Dictionary<String, String> propiedadesProduccion = new Dictionary<String, String>();
        Dictionary<String, String> propiedadesPreProduccion = new Dictionary<String, String>();

        public frmConfig(String path)
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            this.TopMost = true;
            this.path = path;

            propiedadesDesarrollo.Add("mqmanager","QMGR_DEV");
            propiedadesDesarrollo.Add("syncurl", "http://172.17.103.155:7801");
            propiedadesDesarrollo.Add("mqserver", "172.17.103.155");

            propiedadesSQA.Add("mqmanager", "QMGR_SQA");
            propiedadesSQA.Add("syncurl", "http://172.17.103.156:7800");
            propiedadesSQA.Add("mqserver", "172.17.103.156");

            propiedadesProduccion.Add("mqmanager", "QMGR_PRO");
            propiedadesProduccion.Add("syncurl", "http://172.17.105.155:7800");
            propiedadesProduccion.Add("mqserver", "172.17.105.155");

            propiedadesPreProduccion.Add("mqmanager", "QMGR_PP");
            propiedadesPreProduccion.Add("syncurl", "http://172.17.103.156:7800");
            propiedadesPreProduccion.Add("mqport", "10884");
            propiedadesPreProduccion.Add("mqserver", "172.17.103.156");

        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                cmbPorts.Items.Add(port);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            bool valid = false;
            if (!String.IsNullOrEmpty(txtDEID.Text) && !String.IsNullOrEmpty(cmbPorts.Text))
            {
                Regex regex = new Regex(@"^\d{6}$");
                Match match = regex.Match(txtDEID.Text);
                valid = match.Success;                
            }

            if (!valid)
            {
                MessageBox.Show("El ID de la boveda no esta en formato correcto o el puerto esta vacio", "Informacion Invalida");
            } else
            {
                fillConfig();
                fillxml();
                createService();

                this.DialogResult = DialogResult.Yes;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void fillConfig()
        {
            //System.Diagnostics.Debugger.Launch();
            Dictionary<String, String> propiedades = new Dictionary<string, string>();
            if (cmbAmbiente.Text == "DESARROLLO")
            {
                propiedades = propiedadesDesarrollo;
            } else if (cmbAmbiente.Text == "SQA")
            {
                propiedades = propiedadesSQA;
            } else if (cmbAmbiente.Text == "PREPRODUCCION"){
                propiedades = propiedadesPreProduccion;
            } else if (cmbAmbiente.Text == "PRODUCCION")
            {
                propiedades = propiedadesProduccion;
            }

            propiedades.Add("DEID", txtDEID.Text);

            String configFile = path + @"conf\config.properties";
            string[] lines = System.IO.File.ReadAllLines(configFile);
            for (int i = 0;i < lines.Count();i++)
            {
                String line = lines[i];
                string[] field = line.Split('=');

                if (field.Length > 1 && propiedades.ContainsKey(field[0]))
                {
                    lines[i] = String.Format("{0}={1}", field[0], propiedades[field[0]]);
                }
            }
            System.IO.File.WriteAllLines(configFile, lines);
        }

        private void fillxml()
        {
            String xmlFile = path + @"bin\GloryCo.xml";

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode aNodes = doc.SelectNodes("/GlyConfiguration/DLLTEST/PORT")[0];
            XmlAttribute idAttribute = aNodes.Attributes["PORT_NAME"];
            idAttribute.Value = cmbPorts.Text;

            aNodes = doc.SelectNodes("/GlyConfiguration/DLLTEST/PATH")[0];
            idAttribute = aNodes.Attributes["UPLOAD"];
            idAttribute.Value = path + "files\\";

            idAttribute = aNodes.Attributes["DOWNLOAD"];
            idAttribute.Value = path + "files\\";

            // save the XmlDocument back to disk
            doc.Save(xmlFile);
        }
        private void createService()
        {
            Process cmd = new Process();
            cmd.StartInfo.FileName = path + "bin/wrapper.exe";
            cmd.StartInfo.Arguments = "-i ..\\conf\\wrapper.conf";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //MessageBox.Show(cmd.StandardOutput.ReadToEnd());

            cmd = new Process();
            cmd.StartInfo.FileName = @"c:\Windows\System32\net.exe";
            cmd.StartInfo.Arguments = "start de100";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            //MessageBox.Show(cmd.StandardOutput.ReadToEnd());
        }
    }
}
